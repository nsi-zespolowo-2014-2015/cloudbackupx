﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CloudBackup;

using Newtonsoft.Json; // JsonConvert.DeserializeObject
using Newtonsoft.Json.Linq; // JObject

using System.IO;
using System.Windows.Forms; // BrowserFolderDialog

//using Chromium;
//using Chromium.Event;
//using Chromium.Remote;
//using Chromium.Remote.Event;
//using Chromium.WebBrowser;

using mshtml;

using System.ServiceProcess;

using System.Security.Principal;
using System.Diagnostics;
using System.ComponentModel;

namespace CloudBackupGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CBDriversManager driversManager;
        public CBDriver driver;
        public CBConfig config;

        CBConfigFolder configFolder;
        CBConfigDriver configDriver;

        public CBLog log;

        public MainWindow()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = System.Windows.Forms.Application.StartupPath;
                proc.FileName = System.Windows.Forms.Application.ExecutablePath;
                proc.Verb = "runas";
                try
                {
                    Process.Start(proc);
                }
                catch
                {
                    //System.Windows.Forms.Application.Exit();
                    this.Close();
                    return;
                }
                //System.Windows.Forms.Application.Exit();
                this.Close();
            }
            else
            {

                this.log = new CBLog("CBGUI");

                String funcName = "Constructor()";

                this.log.Debug(funcName + ": [BEGIN]");

                //--------------------------------------

                InitializeComponent();

                //--------------------------------------
                // musi powstawc przed comboboxSelectDriver.SelectedIndex = 0;
                // bo to wywola funkcje, ktora wymaga juz instniejace `config`

                config = new CBConfig();

                //--------------------------------------
                // pobranie listy dostepnych folderow
                //--------------------------------------

                List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();

                foreach (CBConfigFolder folder in foldersConfigs)
                {
                    this.log.Debug(funcName + ": " + folder.Name + " [ " + folder.SrcFolderName + " => " + folder.DstFolderName + " ](" + folder.DriverName + ")");
                    comboboxFolders.Items.Add(folder.Name);
                }

                comboboxFolders.SelectedIndex = 0;

                //--------------------------------------
                // pobranie konfiguracji folderu
                // nastapi pod wplywem comboboxFolders.SelectedIndex = 0;
                //--------------------------------------

                // --- nic tu nie trzeba ---

                //--------------------------------------
                // pobranie listy dostepnych driverow
                //--------------------------------------

                driversManager = new CBDriversManager();

                List<String> driversNames = driversManager.GetDriversNames();

                foreach (String driveName in driversNames)
                {
                    this.log.Debug(funcName + ": " + driveName);
                    comboboxDrivers.Items.Add(driveName);
                    comboboxFolderDrivers.Items.Add(driveName);
                }

                comboboxDrivers.SelectedIndex = 0;
                comboboxFolderDrivers.SelectedIndex = 0;

                //--------------------------------------
                // pobranie konfiguracji drivera i jego konfiguracji
                // nastapi pod wplywem comboboxDrivers.SelectedIndex = 0;
                //--------------------------------------

                // --- nic tu nie trzeba ---

                //--------------------------------------

                //_webBrowser = new System.Windows.Forms.WebBrowser();
                //formhost.Child = _webBrowser;

                //_webBrowser.Navigated += new WebBrowserNavigatedEventHandler(browser_Navigated);
                //_webBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);

                //--------------------------------------

                CheckServiceStatus();

                //--------------------------------------
                this.log.Debug(funcName + ": [END]");
            }
        }

        //--------------------------------------------------
        // AUTORYZACJA
        //--------------------------------------------------

        private void buttonPobierzURL_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonPobierzURL_Click()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            String result = driver.GetAuthorizationUrl();

            if (result.Equals(""))
            {
                textboxWynikURL.Text = "- brak -";
            }
            else
            {
                textboxWynikURL.Text = result;
                textboxBrowserUrl.Text = result;

                browser.Navigate(result);

                //_webBrowser.Navigate(result);

                //CfxRuntime.LibCefDirPath = @"C:\Users\user\Desktop\repo\projekt\CloudBackupProjects\CloudBackupGUI\bin\x86\Debug";
                //ChromiumWebBrowser.Initialize();
                //ChromiumWebBrowser bro = new ChromiumWebBrowser();

                //Form f = new Form();
                //f.Size = new System.Drawing.Size(600, 600);

                //bro.Dock = DockStyle.Fill;
                //bro.Parent = f;
                //bro.LoadUrl(result);
                //f.Show();                
            }

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void buttonPobierzTokeny_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonPobierzTokeny_Click()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            String result = driver.GetToken(textboxKod.Text);

            if (result.Equals(""))
            {
                labelKomunikat.Content = "- brak -";
            }
            else
            {
                try
                {
                    labelKomunikat.Content = "OK";

                    JObject json = JsonConvert.DeserializeObject<JObject>(result);

                    textboxConfigAccessToken.Text = json.GetValue("access_token").ToString();
                    textboxConfigRefreshToken.Text = json.GetValue("refresh_token").ToString();

                    driver.config.AccessToken = json.GetValue("access_token").ToString();
                    // nie kazde API uzywa "refresh_token"
                    driver.config.RefreshToken = json.GetValue("refresh_token").ToString();

                    driver.config.Save();

                    // dla hubic - dostep do OpenStackAPI
                    //driver.GetCredential();
                }
                catch (Exception ex)
                {
                    this.log.Debug(ex.Message);
                }
            }

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        //--------------------------------------------------
        // KONFIGURACJA - DRIVERS
        //--------------------------------------------------

        private void comboboxDrivers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String funcName = "comboboxDrivers_SelectionChanged()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            String selected = (String)comboboxDrivers.SelectedValue;

            this.log.Debug(funcName + ": selected: " + selected);

            //--------------------------------------

            try
            {
                switch (selected)
                {
                    case "Google":
                        this.log.Debug(funcName + ": case: Google");
                        driver = new CBDriverGoogle();
                        driver.config = config.GetDriverConfig("Google");
                        break;

                    case "Dropbox":
                        this.log.Debug(funcName + ": case: Dropbox");
                        driver = new CBDriverDropbox();
                        driver.config = config.GetDriverConfig("Dropbox");
                        break;

                    case "Hubic":
                        this.log.Debug(funcName + ": case: Hubic");
                        driver = new CBDriverHubic();
                        driver.config = config.GetDriverConfig("Hubic");
                        break;
                }

                //--------------------------------------

                //textboxClientID.Text = driver.config.ClientID;
                //textboxClientSecret.Text = driver.config.ClientSecret;
                textboxConfigAccessToken.Text = driver.config.AccessToken;
                textboxConfigRefreshToken.Text = driver.config.RefreshToken;
            }
            catch (Exception ex)
            {
                this.log.Debug(funcName + ":  ex.Message: " + ex.Message);
            }

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        //--------------------------------------------------
        // KONFIGURACJA - FOLDERS
        //--------------------------------------------------

        private void comboboxFolders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String funcName = "comboboxFolders_SelectionChanged()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            String selected = (String)comboboxFolders.SelectedValue;

            this.log.Debug(funcName + ": selectedValue: " + selected);

            List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();

            foreach (CBConfigFolder folder in foldersConfigs)
            {
                if (folder.Name == selected)
                {
                    configFolder = folder;

                    textboxFolderNazwa.Text = configFolder.Name;
                    comboboxFolderDrivers.SelectedValue = configFolder.DriverName;
                    textboxFolderSrcFolder.Text = configFolder.SrcFolderName;
                    textboxFolderDstFolder.Text = configFolder.DstFolderName;
                    checkboxAktywne.IsChecked = (configFolder.Active == "True");
                }
            }

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void buttonFolderSrcFolder_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonFolderSrcFolder_Click()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            DialogResult result = folderBrowserDialog.ShowDialog();
	        
            if (result == System.Windows.Forms.DialogResult.OK)
	        {
                textboxFolderSrcFolder.Text = folderBrowserDialog.SelectedPath;
	        }

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void buttonFolderZapisz_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonFolderZapisz_Click()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            configFolder.Name = textboxFolderNazwa.Text;
            configFolder.DriverName = (String)comboboxFolderDrivers.SelectedValue;
            configFolder.SrcFolderName = textboxFolderSrcFolder.Text;
            configFolder.DstFolderName = textboxFolderDstFolder.Text;
            configFolder.Active = (checkboxAktywne.IsChecked == true) ? "True" : "False";

            try
            {
                System.IO.Directory.CreateDirectory(textboxFolderSrcFolder.Text);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Uwaga: problem z utworzeniem folderu: " + textboxFolderSrcFolder.Text + " : " + ex.Message);
            }

            config.Save();

            System.Windows.MessageBox.Show("Zapisane");

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void buttonDriverZapisz_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonDriverZapisz_Click()";

            this.log.Debug(funcName + ": [BEGIN]");

            //--------------------------------------

            driver.config.AccessToken = textboxConfigAccessToken.Text;
            driver.config.RefreshToken = textboxConfigRefreshToken.Text;

            config.Save();

            System.Windows.MessageBox.Show("Zapisane");

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void buttonFolderDodaj_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonFolderDodaj_Click()";

            System.Xml.Linq.XElement nowy = new System.Xml.Linq.XElement("Add");
            config.section_folders.Add(nowy);
            //nowy.SetAttributeValue("Nazwa", "Alfa");

            CBConfigFolder folderConfig = new CBConfigFolder(config, nowy);
            folderConfig.Name = "Nowe";
            folderConfig.DriverName = "Google";
            folderConfig.Active = "True";

            config.Save();

            List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();

            comboboxFolders.Items.Clear();

            foreach (CBConfigFolder folder in foldersConfigs)
            {
                this.log.Debug(funcName + ": " + folder.Name + " [ " + folder.SrcFolderName + " => " + folder.DstFolderName + " ](" + folder.DriverName + ")");
                comboboxFolders.Items.Add(folder.Name);
            }

            comboboxFolders.SelectedIndex = comboboxFolders.Items.Count - 1;
        }

        private void buttonFolderUsun_Click(object sender, RoutedEventArgs e)
        {
            String funcName = "buttonFolderUsun_Click()";

            config.Save();

            List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();
            String wybrame = comboboxFolders.SelectedValue.ToString();

            comboboxFolders.Items.Clear();

            foreach (CBConfigFolder folder in foldersConfigs)
            {
                this.log.Debug(funcName + ": " + folder.Name + " [ " + folder.SrcFolderName + " => " + folder.DstFolderName + " ](" + folder.DriverName + ")");
                comboboxFolders.Items.Add(folder.Name);
            }

            comboboxFolders.SelectedValue = wybrame;
        }

        //--------------------------------------------------
        // WEBBROWSER
        //--------------------------------------------------

        private void browser_Navigated(object sender, NavigationEventArgs e)
        {
            this.log.Debug("(WPF) browser_Navigated(): uri: " + e.Uri.AbsoluteUri);

            if (e.Uri.AbsoluteUri.StartsWith("https://accounts.google.com/o/oauth2/approval"))
            {
                HTMLDocument doc = browser.Document as HTMLDocument;
                if( doc == null )
                {
                    System.Windows.MessageBox.Show("Problem z pobraniem kodu ze strony");
                }
                else
                {
                    this.log.Debug("(WPF) browser_Navigated(): Document: " + doc);
                    this.log.Debug("(WPF) browser_Navigated(): Document: " + doc.forms);
                    this.log.Debug("(WPF) browser_Navigated(): Document: " + doc.getElementById("code").innerText);
                }
            }
        }

        //private void browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        //{
        //    this.log.Debug("(WinForm) browser_Navigated(): url: " + e.Url.AbsoluteUri);

        //    if(e.Url.AbsoluteUri.StartsWith("https://accounts.google.com/o/oauth2/approval"))
        //    {
        //        this.log.Debug("(WinForm) browser_Navigated(): Document: " + _webBrowser.Document.ToString());
        //    }
        //}

        //private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        //{
        //    this.log.Debug("(WinForm) browser_DocumentCompleted(): e: " + e.Url);

        //    if (e.Url.AbsoluteUri.StartsWith("https://accounts.google.com/o/oauth2/approval"))
        //    {
        //        this.log.Debug("(WinForm) browser_Navigated(): Document: " + _webBrowser.Document.ToString());
        //    }
        //}

        private void browser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            this.log.Debug("(WPF) browser_LoadCompleted(): e.Content: " + e.Content);

            if (e.WebResponse == null)
            {
                this.log.Debug("(WPF) browser_LoadCompleted(): e.WebResponse == null");
            }
            else
            {
                foreach (String key in e.WebResponse.Headers.AllKeys)
                {
                    this.log.Debug("(WPF) browser_LoadCompleted(): key: " + key + " = " + e.WebResponse.Headers.Get(key));
                }
            }
        }

        //--------------------------------------------------
        // SERVICE
        //--------------------------------------------------

        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();

            if (null != identity)
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            return false;
        }

        //public static Process RunProcess(string name, string arguments)
        //{
        //    string path = System.IO.Path.GetDirectoryName(name);

        //    if (String.IsNullOrEmpty(path))
        //    {
        //        path = Environment.CurrentDirectory;
        //    }

        //    ProcessStartInfo info = new ProcessStartInfo
        //    {
        //        UseShellExecute = true,
        //        WorkingDirectory = path,
        //        FileName = name,
        //        Arguments = arguments
        //    };

        //    if (!IsAdministrator())
        //    {
        //        info.Verb = "runas";
        //    }

        //    try
        //    {
        //        return Process.Start(info);
        //    }

        //    catch (Win32Exception ex)
        //    {
        //        Trace.WriteLine(ex);
        //    }

        //    return null;
        //}

        private void buttonServiceStop_Click(object sender, RoutedEventArgs e)
        {

            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == "CloBacSrv");
            ServiceController cbs = new ServiceController("CloBacSrv");
            
            if (service == null)
            {
                System.Windows.MessageBox.Show("Brak usługi w systemie");
            }
            else
            {
                try 
                {
                    if (service.Status == ServiceControllerStatus.Running)
                    {
                        this.log.Debug("buttonServiceStop_Click(): service.Running: service.Status: " + service.Status);
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped);
                        System.Windows.MessageBox.Show("Zatrzyname");
                    }
                    if (service.Status == ServiceControllerStatus.Paused)
                    {
                        this.log.Debug("buttonServiceStop_Click(): service.Paused: service.Status: " + service.Status);
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        System.Windows.MessageBox.Show("Zatrzyname");
                    }
                }catch(Exception ex){
                    Console.WriteLine(ex.Message);
                    System.Windows.MessageBox.Show(ex.Message);
                }

                CheckServiceStatus();
            }
        }

        private void buttonServiceStart_Click(object sender, RoutedEventArgs e)
        {

            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == "CloBacSrv");
            ServiceController cbs = new ServiceController("CloBacSrv");

            if (service == null)
            {
                System.Windows.MessageBox.Show("Brak usługi w systemie");
            }
            else
            {
                try
                {
                    if (service.Status == ServiceControllerStatus.Stopped)
                    {
                        this.log.Debug("buttonServiceStart_Click(): service.Stopped: service.Status: " + service.Status);
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        System.Windows.MessageBox.Show("Uruchomiony");
                    }
                    if (service.Status == ServiceControllerStatus.Paused)
                    {
                        this.log.Debug("buttonServiceStart_Click(): service.Paused: service.Status: " + service.Status);
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        System.Windows.MessageBox.Show("Uruchomiony");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    System.Windows.MessageBox.Show(ex.Message);
                }

                CheckServiceStatus();
            }
        }

        private void buttonServiceRestart_Click(object sender, RoutedEventArgs e)
        {

            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == "CloBacSrv");
            ServiceController cbs = new ServiceController("CloBacSrv");

            if (service == null)
            {
                System.Windows.MessageBox.Show("Brak usługi w systemie");
            }
            else
            {
                int timeoutMilliseconds = 2000;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                try
                {
                    this.log.Debug("buttonServiceRestart_Click(): service.Stop(): service.ServiceName: " + service.ServiceName);
                    this.log.Debug("buttonServiceRestart_Click(): service.Stop(): service.CanStop: " + service.CanStop);
                    this.log.Debug("buttonServiceRestart_Click(): service.Stop(): service.Status: " + service.Status);

                    if (service.Status == ServiceControllerStatus.Running)
                    {
                        this.log.Debug("buttonServiceRestart_Click(): service.Running: service.Status: " + service.Status);
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped);
                        //service.Pause();
                        //service.WaitForStatus(ServiceControllerStatus.Paused);
                        //System.Windows.MessageBox.Show("RUN -> STOP");
                    }
                    if (service.Status == ServiceControllerStatus.Stopped)
                    {
                        this.log.Debug("buttonServiceRestart_Click(): service.Stopped: service.Status: " + service.Status);
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        //System.Windows.MessageBox.Show("STOP -> RUN");
                        System.Windows.MessageBox.Show("Zrestartowane");
                    }
                    if (service.Status == ServiceControllerStatus.Paused)
                    {
                        this.log.Debug("buttonServiceRestart_Click(): service.Paused: service.Status: " + service.Status);
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        //System.Windows.MessageBox.Show("PAUSE -> RUN");
                        System.Windows.MessageBox.Show("Zrestartowane");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    System.Windows.MessageBox.Show(ex.Message);
                }

                CheckServiceStatus();
            }
        }

        private void CheckServiceStatus()
        {
            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == "CloBacSrv");

            String status = "nieznany";

            if (service == null)
            {
                status = "brak usługi";
            }
            else
            {
                switch(service.Status)
                {
                    case ServiceControllerStatus.Running:
                        status = "uruchomiona";
                        break;
                    case ServiceControllerStatus.Stopped:
                        status = "zatrzymana";
                        break;
                    case ServiceControllerStatus.Paused:
                        status = "spauzowana";
                        break;
                    case ServiceControllerStatus.StopPending:
                        status = "w trakcie zatrzymywania";
                        break;
                    case ServiceControllerStatus.StartPending:
                        status = "w trakcie uruchamiania";
                        break;
                    default:
                        status = "inna";
                        break;
                }
            }

            if(IsAdministrator())
                labelServiceStatus.Content = "Usługa: " + status + "  (Użytkownik: administrator)";
            else
            {
                labelServiceStatus.Content = "Usługa: " + status + "  (Użytkownik: normalny)";
            }
        }
    }
}
