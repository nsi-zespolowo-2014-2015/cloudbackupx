﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using CloudBackup;

namespace CloudBackupService
{
    class CBService : ServiceBase
    {
        CBLog log;
        CBMonitor monitor;

        public const string NazwaUslugi = "CloBacSrv";
        //public const string NazwaWyswietlana = "CloBacSrv";
        public const string NazwaWyswietlana = "CloudBackup Service";
        public const string Opis = "Usługa CloudBackup - backup na dyski chmurowe";

        public CBService()
        {
            this.log = new CBLog("CBService");

            this.ServiceName = NazwaUslugi;
            this.CanPauseAndContinue = true;
            this.CanStop = true;
            this.AutoLog = true;

            this.monitor = new CBMonitor();
        }

        protected override void OnStart(string[] args)
        {
            this.log.Info("OnStart()");
            this.monitor.Start();
        }

        protected override void OnStop()
        {
            this.monitor.Stop();
            this.log.Info("OnStop()");
        }
    }
}
