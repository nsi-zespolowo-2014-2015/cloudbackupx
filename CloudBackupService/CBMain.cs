﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Configuration.Install;
using System.Reflection;

using CloudBackup;

namespace CloudBackupService
{
    class CBMain
    {
        static CBService service;
        static CBLog log;

        public static void Main(String[] args)
        {
            log = new CBLog("CBMain");
            log.Info("[BEGIN]");

            if (Environment.UserInteractive)
            {
                log.Debug("Uruchamiam CloudBackup");

                String parameter = String.Concat(args);
                log.Debug("parametr: " + parameter);
                
                switch (parameter)
                {
                    case "--install":
                    case "-i":
                    case "/i":
                        //Console.WriteLine("Instalowanie CloudBackup");
                        ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                        break;
                    case "--uninstall":
                    case "-u":
                    case "/u":
                        //Console.WriteLine("Odinstalowanie CloudBackup");
                        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                        break;
                    default:
                        Console.WriteLine("CloudBackup.exe [/i|-i|--install] [/u|-u|--uninstall]");
                        break;
                }
            }
            else
            {
                service = new CBService();
                //service.Start();
                //service.Stop();

                ServiceBase.Run(service);
            }

            log.Info("[END]");
        }
    }
}
