﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudBackupService
{
    public class CBLog
    {
        String name;
        System.IO.StreamWriter file;
        System.Diagnostics.EventLog eventlog;

        public CBLog(String name)
        {
            this.name = name;

            // configure the event log instance to use this source path

            this.eventlog = new System.Diagnostics.EventLog();
            this.eventlog.Source = "CloudBackup Service";
            this.eventlog.Log = "CloudBackupLog"; //"MyLog"; // 

            //// create an event source, specifying the path of a log that 
            //// does not currently exist to create a new, custom log 
            ////if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            ////{
            ////    System.Diagnostics.EventLog.CreateEventSource(
            ////        "MySource", "MyLog");
            ////}
            //// configure the event log instance to use this source path
            ////eventLog1.Source = "MySource";

            //if (!System.Diagnostics.EventLog.SourceExists(this.eventlog.Source))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        this.eventlog.Source, this.eventlog.Log);
            //}
            //// configure the event log instance to use this source path
            ////eventLog1.Source = this.eventlog.Source;

            //this.file = new System.IO.StreamWriter(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cb.log.txt"), true);
        }

        public void Add(String message, String type="INFO")
        {
            //Console.WriteLine("{0:u} - {1:s} - {2:s}: {3:s} ", DateTime.Now, type, this.path, message);
            //------------------

            Console.WriteLine("{0:u} - {2:s} - {3:s} ", DateTime.Now, type, this.name, message);

            String text = DateTime.Now + " - " + type + " - " + this.name + ": " + message;

            //this.eventlog.WriteEntry(text);
            
            //Console.WriteLine(text);
            //this.file.WriteLine(text);
            //this.file.Flush();
        }

        public void Info(String message)
        {
            this.Add(message, "INFO");
        }

        public void Debug(String message)
        {
            this.Add(message, "DEBUG");
        }
    }


}
