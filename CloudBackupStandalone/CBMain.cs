﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CloudBackup;

namespace CloudBackupStandalone
{
    class CBMain
    {
        static CBService server;
        static CBLog log;

        public static void Main()
        {
            log = new CBLog("CBMain");
            log.Info("[BEGIN]");

            server = new CBService();
            server.Start();

            Console.WriteLine("\nWciśniej klawisz Enter aby zakończyć program.");
            Console.Read();

            server.Stop();

            log.Info("[END]");
        }
    }
}
