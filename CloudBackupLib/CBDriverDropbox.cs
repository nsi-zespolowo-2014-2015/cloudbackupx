﻿using System;
using System.Collections.Generic; // List
using System.Net;  // WebRequest, WebResponse
using System.Text; // Encoding
using System.IO;   // Stream

using Newtonsoft.Json; // JsonConvert.DeserializeObject
using Newtonsoft.Json.Linq;

namespace CloudBackup
{
    /// <summary>
    /// Klasa odpowiedzialna za komunikację z dyskiem Dropbox
    /// </summary>
    public class CBDriverDropbox : CBDriver
    {
        public CBDriverDropbox(CBConfigDriver config = null)
        {            
            this.name = "Dropbox";
            this.log = new CBLog("CBDriver" + name);

            this.config = config;

            this.authUrl    = "https://www.dropbox.com/1/oauth2/authorize";
            this.tokenUrl   = "https://www.dropbox.com/1/oauth2/token";
            this.commandUrl = "https://api.dropbox.com/1";
            this.contentUrl = "https://api-content.dropbox.com/1";

            this.log.Debug("constructor()");

            // obejscie problemu z nieporawnymi certyfikatami
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            //this.config = config;
            //GetConfig();

            if (config != null && config.ExpireTime < DateTime.Now)
            {
                RefreshToken(config.RefreshToken);
            }
        }

        //---------------------------------------------------------------------------------------

        /// <summary>
        /// Pobieranie listy komend nie wymagającyh argumentów przy wywołaniu
        /// </summary>
        /// <returns>lista koemnd w postaci adresu URL</returns>
        public override String[] GetCommands()
        {
            String[] commands = {
                "/account/info",
                "/metadata/auto/",
            };

            return commands;
        }

        //---------------------------------------------------------------------------------------
        // Pobieranie adresu do strony autoryzacji dostępu aplikacji do konta użytkownika
        //---------------------------------------------------------------------------------------

        /// <summary>
        /// Pobranie adresu strony do autoryzacji przez użytkownika dostępu do jego danych
        /// </summary>
        /// <returns>adres URL</returns>
        public override String GetAuthorizationUrl()
        {
            String funcName = "GetAuthorizationUrl()";

            String url = this.authUrl +             
                "?client_id=" + config.ClientID +
                "&response_type=code" + 
                "&redirect_uri=" + config.RedirectURL;

            this.log.Debug(funcName + ": url: " + url);

            return url;
        }

        /// <summary>
        /// Pobranie tokenu dostępu i odświeżenia
        /// </summary>
        /// <param name="code">kod dostarczony podczas procesu autoryzacji</param>
        /// <returns>token dostępu i odświeżenia (format JSON)</returns>
        public override String GetToken(String code)
        {
            String funcName = "GetToken()";

            this.log.Debug(funcName + ": code: " + code);

            Dictionary<String, String> postData = new Dictionary<String, String>();
            postData.Add("client_id", config.ClientID);
            postData.Add("client_secret", config.ClientSecret);
            postData.Add("code", code);
            postData.Add("grant_type", "authorization_code");
            postData.Add("redirect_uri", config.RedirectURL);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");

            String result = REQUEST("POST", tokenUrl, null, postData, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            //-----

            if(result != null && result != "")
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                if (json.GetValue("access_token") != null && json.GetValue("access_token").ToString() != "")
                {
                    config.AccessToken = json.GetValue("access_token").ToString();
                    config.AccessTokenType = "Bearer"; //json.GetValue("token_type").ToString(); // przysyla 'bearer' a wymagane jest 'Bearer'
                    config.Save();
                }
            }

            //-----

            return result;
        }

        //------------------------------------------------------------------------
        // FOLDERS
        //------------------------------------------------------------------------

        /// <summary>
        /// Tworzenie nowego katalogu
        /// </summary>
        /// <param name="path">pełna ścieżka tworzonego katalogu</param>
        /// <param name="parent">ID katalogu początkowego (domyślnie: auto)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String CreateFolder(String path, String parent = "auto")
        {
            String funcName = "CreateFolder()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": path: " + path);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("root", parent);
            getData.Add("path", path);

            String result = REQUEST("POST", commandUrl + "/fileops/create_folder", getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        /// <summary>
        /// Zmiana nazwy katalogu
        /// </summary>
        /// <param name="oldPath">pełna ścieżka starego katalogu</param>
        /// <param name="newPath">pełna ścieżka nowego katalogu</param>
        /// <param name="newName">sama nazwa nowego katalogu</param>
        /// <param name="parent">katalog początkowy (domyślnie: auto)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String RenameFolder(String oldPath, String newPath, String newName, String parent = "auto")
        {
            String funcName = "RenameFolder()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("root", parent);
            getData.Add("from_path", oldPath);
            getData.Add("to_path", newPath);
             
            String result = REQUEST("POST", commandUrl + "/fileops/move", getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        /// <summary>
        /// Kasowanie folderu
        /// </summary>
        /// <param name="path">pełna ścieżna kasowanego katalogu</param>
        /// <param name="parent">katalog początkowy (domyślnie: auto)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String DeleteFolder(String path, String parent = "auto")
        {
            String funcName = "DeleteFolder()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": path: " + path);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("root", parent);
            getData.Add("path", path);

            String result = REQUEST("POST", commandUrl + "/fileops/delete", getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        //------------------------------------------------------------------------
        // FILES
        //------------------------------------------------------------------------

        /// <summary>
        /// Wysyłanie pliku na dysk
        /// </summary>
        /// <param name="filename">lokalna pełna ścieżka do pliku</param>
        /// <param name="remoteFilename">nazwa (ścieżka) na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String UploadFile2(String filename, String remoteFilename, String parent = "auto")
        {
            String funcName = "UploadFile2()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": " + filename + " => " + remoteFilename);

            byte[] rawData;

            Dictionary<String, String> getData = new Dictionary<String, String>();

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
            headers.Add("Content-Type", "application/octet-stream");

            try
            {
                rawData = File.ReadAllBytes(filename);
            }
            catch (Exception ex)
            {
                Dictionary<String, String> jsonData = new Dictionary<String, String>();
                jsonData.Add("error", "true");
                jsonData.Add("path", funcName);
                jsonData.Add("message", ex.Message);
                jsonData.Add("StatusCode", "0");

                // zwrocenie bledu
                return JsonConvert.SerializeObject(jsonData);
            }

            String result = REQUEST("POST", this.contentUrl + "/files_put/auto/"+remoteFilename, getData, null, headers, rawData);

            return result;
        }

        /// <summary>
        /// Zmiana nazwy pliku
        /// </summary>
        /// <param name="oldPath">stara pełna ścieżka</param>
        /// <param name="newPath">nowa pełna ścieżka</param>
        /// <param name="newName">nowa sama nazwa pliku </param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String RenameFile(String oldPath, String newPath, String newName, String parent = "auto")
        {
            String funcName = "RenameFile()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("root", parent);
            getData.Add("from_path", oldPath);
            getData.Add("to_path", newPath);

            String result = REQUEST("POST", commandUrl + "/fileops/move", getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        /// <summary>
        /// Kasowanie pliku
        /// </summary>
        /// <param name="path">pełna ścieżka na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String DeleteFile(String name, String parent = "auto")
        {
            String funcName = "DeleteFile()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("root", parent);
            getData.Add("path", name);

            String result = REQUEST("POST", commandUrl + "/fileops/delete", getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            //JObject json = JsonConvert.DeserializeObject<JObject>(result);

            //if (result == "")
            //{
            //    return "{\"ok\":\"true\",\"message\":\"deleted " + path + "\"}";
            //}

            return result;
        }

        /// <summary>
        /// Pobieranie pliku z dysku
        /// </summary>
        /// <param name="remotePath">zdalna pełna ścieżka</param>
        /// <param name="localPath">lokalna pełna ścieżka</param>
        /// <returns></returns>
        public override String DownloadFile(String path, String filename="tmp")
        {
            String funcName = "DownloadFile()";

            this.log.Debug(funcName + ": path: " + path);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("GET", this.contentUrl + "/files/auto/" + path, null, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            try
            {
                //File.WriteAllBytes(filename, result);
            }
            catch (Exception ex)
            {
                Dictionary<String, String> jsonData = new Dictionary<String, String>();
                jsonData.Add("error", "true");
                jsonData.Add("path", funcName);
                jsonData.Add("message", ex.Message);
                jsonData.Add("StatusCode", "0");

                // zwrocenie bledu
                return JsonConvert.SerializeObject(jsonData);
            }

            return result;
        }

        //------------------------------------------------------------------------
        // INNE
        //------------------------------------------------------------------------

        /// <summary>
        /// Listowanie zawartości katalogu
        /// </summary>
        /// <param name="name">pełna ścieżka</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String ListFolder(String name, String parent = "auto")
        {
            String funcName = "ListFolder()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/metadata/auto/" + name, null, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        //------------------------------------------------------------------------

        /// <summary>
        /// Szukanie pliku/plików
        /// </summary>
        /// <param name="name">(częściowa) nazwa pliku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String FindFiles(String name, String parent = "")
        {
            String funcName = "FindFiles()";

            if (parent == null) parent = "auto";

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parent);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("query", name);

            String result = REQUEST("GET", commandUrl + "/search/auto/" + parent, getData, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        //------------------------------------------------------------------------

        /// <summary>
        /// Listowanie (wszystkich) plików 
        /// </summary>
        /// <returns></returns>
        public override String ListFiles()
        {
            String funcName = "ListFiles()";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", "Bearer " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", null, null, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }
    }
}
