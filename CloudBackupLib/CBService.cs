﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudBackup
{
    /// <summary>
    /// Usługa systemowa 
    /// </summary>
    public class CBService
    {
        CBLog log;
        CBMonitor monitor;

        public CBService()
        {
            this.log = new CBLog("CBService");

            this.monitor = new CBMonitor();
        }

        /// <summary>
        /// Startowanie usługi - wywołanie monitora
        /// </summary>
        public void Start()
        {
            this.log.Info("Start()");
            this.monitor.Start();
        }

        /// <summary>
        /// Zatrzymywanie usługi - zatrzymywanie monitora
        /// </summary>
        public void Stop()
        {
            this.monitor.Stop();
            this.log.Info("Stop()");
        }
    }
}
