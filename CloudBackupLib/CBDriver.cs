﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; // Encoding
using System.Net;  // ServicePointManager, WebRequest, WebResponse
using System.IO;   // Stream
using System.Web;  // HttpUtility.UrlEncode

using Newtonsoft.Json; // JsonConvert.DeserializeObject

namespace CloudBackup
{
    /// <summary>
    /// Klasa obsługująca dyski - wersja ogólna
    /// </summary>   
    public class CBDriver
    {
        public CBLog log;

        public String name;

        public CBConfigDriver config;

        protected String authUrl = "";
        protected String tokenUrl = "";
        protected String commandUrl = "";
        protected String contentUrl = ""; // Dropbox
        

        public String accessToken = null;
        public String refreshToken = null;
        public DateTime czas_waznosci;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="config">obiekt z konfiguracja drivera</param>
        public CBDriver(CBConfigDriver config=null)
        {
            this.name = "BASE";
            this.log = new CBLog("CBDriver" + name);

            this.config = config;

            this.log.Debug("Użycie 'ServicePointManager' aby obejsc problem z niepoprawnymi certyfikatami");

            // obejscie problemu z niepoprawnymi certyfikatami
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
        }

        /// <summary>
        /// Ustawienie konfiguracji drivera
        /// </summary>
        /// <param name="config">obiekt z konfiguracja drivera</param>
        public void SetConfig(CBConfigDriver config)
        {
            this.config = config;

            if (config != null && config.ExpireTime < DateTime.Now)
            {
                RefreshToken(config.RefreshToken);
            }
        }

        /// <summary>
        /// Niewykorzystywane
        /// </summary>
        public void Start()
        {
            this.log.Info("Start");
        }

        /// <summary>
        /// Niewykorzystywane
        /// </summary>
        public void Stop()
        {
            this.log.Info("Stop");
        }

        /// <summary>
        /// Akceptowanie wszelkich certyfikatów - zwłaszcza tych niepoprawnych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certification"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            // obejscie problemu z nieporawnymi certyfikatami
            return true;
        }

        /// <summary>
        /// Metoda GET protokołu HTTP dla podstawowych komend nie wymagających parametów 
        /// </summary>
        /// <param name="command">komenda do wysłania</param>
        /// <param name="accessToken">token dostępu</param>
        /// <param name="printConsole">czy wypisywać komunikaty</param>
        /// <returns></returns>
        public String Get(String command, String accessToken, Boolean printConsole=false)
        {
            this.log.Debug(this.GetType().Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name);

            String funcName = "Get()";

            // wysylanie element i odbieranie zapytania
            HttpWebRequest request;
            HttpWebResponse response;

            // czytanie danych ze strumienia
            Stream dataStream;
            StreamReader reader;

            // pelny adres komendy
            String url = commandUrl + command;
            //url = "http://httpbin.org/get";
            
            this.log.Debug(funcName + ": URL: " + url);

            try
            {
                // tworzenie zapytania z autoryzacja
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers.Set("Authorization", accessToken);

                this.log.Debug(funcName + ": Authorization: " + accessToken);

                // pobieranie odpowiedzi
                response = (HttpWebResponse)request.GetResponse();

                this.log.Debug(funcName + ": StatusCode: " + response.StatusCode);

                // sprawdzanie stanu odpowiedzi
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // pobranie dostepu do strumienia
                    dataStream = response.GetResponseStream();

                    // zamiana na strumien tekstowy
                    reader = new StreamReader(dataStream);

                    // pobranie wszystkich danych ze strumienia
                    String result = reader.ReadToEnd();

                    // zamykanie wszystkiego
                    reader.Close();
                    dataStream.Close();
                    response.Close();

                    if (printConsole)
                    {
                        this.log.Debug(funcName + ": Result: " + result);
                    }

                    // oddanie danych ze strumienia
                    return result;
                }
                else
                {
                    Dictionary<String, String> result = new Dictionary<String, String>();

                    result.Add("error", "true");
                    result.Add("path", funcName);
                    result.Add("StatusCode", response.StatusCode.ToString());

                    // zwrocenie bledu
                    return JsonConvert.SerializeObject(result);
                }
            }
            catch (WebException ex)
            {
                this.log.Debug(funcName + ": Exception: Message: " + ex.Message);

                // proba pobrania odpowiedzi
                HttpWebResponse errorResponse = (HttpWebResponse)ex.Response;

                // proba pobrania statusu
                String errorStatus = (errorResponse == null ? "Null" : errorResponse.StatusCode.ToString());

                this.log.Debug(funcName + ": Exception: errorResponse: " + errorStatus);

                Dictionary<String, String> result = new Dictionary<String, String>();

                result.Add("error", "true");
                result.Add("path", funcName);
                result.Add("StatusCode", errorStatus);

                // zwrocenie bledu
                return JsonConvert.SerializeObject(result);
            }
        }

        /// <summary>
        /// Dowolne zapytanie HTTP na podstawie dostarczonych argumentów
        /// </summary>
        /// <param name="method">typ metody (GET, POST, PUT, DELETE, itp)</param>
        /// <param name="url">wywoływany adres URL</param>
        /// <param name="getData">argumenty przekazywane w adresie z uzyciem ? i &</param>
        /// <param name="postData">argumenty przekazywane w ciele zapytania </param>
        /// <param name="headers">nagłowki zapytania</param>
        /// <param name="rawData">dane (już w postaci ciągu bajtów) przekazywane w ciele zapytania</param>
        /// <returns></returns>
        public String REQUEST(String method, String url, Dictionary<String, String> getData, Dictionary<String, String> postData, Dictionary<String, String> headers, byte[] rawData)
        {
            String funcName = "REQUEST";

            //url = "http://httpbin.org/post"; // adres do testow
           
            this.log.Debug(funcName + ": method: " + method);
            this.log.Debug(funcName + ": url: " + url);

            // tworzenie getString z danych przekazywanymi w URL

            String getString = "";

            if (getData != null)
            {
                foreach (String key in getData.Keys)
                {
                    if (getString.Length > 0) 
                        getString += "&";

                    getString += 
                        String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(getData[key]));
                }
            }

            if (getString != "")
            {
                url += "?" + getString;
            }

            this.log.Debug(funcName + ": getString: " + getString);
            this.log.Debug(funcName + ": url+getString: " + url);

            // tworzenie postString z danych przekazywanymi w BODY

            String postString = "";

            if (postData != null)
            {
                foreach (String key in postData.Keys)
                {
                    if (postString.Length > 0)
                        postString += "&";

                    postString +=
                        String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(postData[key]));
                }
            }

            this.log.Debug(funcName + ": postString: " + postString);

            byte[] byteArray = null;

            if (postString.Length > 0)
            {
                byteArray = Encoding.UTF8.GetBytes(postString);
                this.log.Debug(funcName + ": byteArray: " + byteArray.Length);
            }
            else if (rawData != null)
            {
                byteArray = rawData;
                this.log.Debug(funcName + ": byteArray: " + byteArray.Length);
            }


            try
            {
                // tworzenie zapytania POST z danymi 
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;

                // dodawanie nagłówków 
                if (headers != null)
                {
                    foreach (String key in headers.Keys)
                    {
                        if (key == "Content-Type")
                        {
                            request.ContentType = headers[key];
                        }
                        else if (key == "Content-Length")
                        {
                            request.ContentLength = Convert.ToInt32(headers[key]);
                        }
                        else
                        {
                            request.Headers.Set(key, headers[key]);
                        }
                        this.log.Debug(funcName + ": header: " + key + " = " + headers[key]);
                    }
                }

                Stream dataStream;

                if (byteArray != null)
                {
                     this.log.Debug(funcName + ": rawData.Length: " + byteArray.Length);

                    // dodawanie danych do BODY

                    request.ContentLength = byteArray.Length;

                    // pobranie dostepu do strumienia
                    dataStream = request.GetRequestStream();

                    // wyslanie wszystkich danych do strumienia
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    // zamkniecie strumienia
                    dataStream.Close();
                }

                // pobieranie odpowiedzi
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                this.log.Debug(funcName + ": response.StatusCode: " + response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // pobranie dostepu do strumienia
                    dataStream = response.GetResponseStream();

                    // zamiana na strumien tekstowy
                    StreamReader reader = new StreamReader(dataStream);

                    // pobranie wszystkicj danych ze strumienia
                    String result = reader.ReadToEnd();

                    // zamykanie wszystkiego
                    reader.Close();
                    dataStream.Close();
                    response.Close();

                    return result;
                }
                else
                {
                    Dictionary<String, String> result = new Dictionary<String, String>();

                    result.Add("error", "true");
                    result.Add("path", "REQUEST");
                    result.Add("response.StatusCode", response.StatusCode.ToString());
                    result.Add("method", method);
                    result.Add("url", url);
                    result.Add("getString", getString == null ? "" : getString);
                    result.Add("postString", postString == null ? "" : postString);
                    result.Add("headers", headers == null ? "" : headers.ToString());
                    result.Add("rawData", rawData == null ? "" : rawData.ToString());

                    // zwrocenie bledu
                    return JsonConvert.SerializeObject(result);
                }
            }
            catch (WebException ex)
            {
                this.log.Debug(funcName + ": Exception.Message: " + ex.Message);

                // proba pobrania odpowiedzi
                HttpWebResponse errorResponse = (HttpWebResponse)ex.Response;

                // proba pobrania statusu
                String errorStatus = (errorResponse == null ? "Null" : errorResponse.StatusCode.ToString());

                this.log.Debug(funcName + ": Exception.errorStatus: " + errorStatus);

                Dictionary<String, String> result = new Dictionary<String, String>();

                result.Add("error", "true");
                result.Add("path", "REQUEST");
                result.Add("errorResponse.StatusCode", errorStatus);
                result.Add("method", method);
                result.Add("url", url);
                result.Add("getString", getString == null ? "" : getString);
                result.Add("postString", postString == null ? "" : postString);
                result.Add("headers", headers == null ? "" : headers.ToString());
                result.Add("rawData", rawData == null ? "" : rawData.ToString());


                // zwrocenie bledu
                return JsonConvert.SerializeObject(result);
            }
        }

        /// <summary>
        /// Zapytanie HTTP zwracające adres URL dla kolejnego zapytania 
        /// </summary>
        /// <param name="method">typ metody (GET, POST, PUT, DELETE, itp)</param>
        /// <param name="url">wywoływany adres URL</param>
        /// <param name="getData">argumenty przekazywane w adresie z uzyciem ? i &</param>
        /// <param name="postData">argumenty przekazywane w ciele zapytania </param>
        /// <param name="headers">nagłowki zapytania</param>
        /// <param name="rawData">dane (już w postaci ciągu bajtów) przekazywane w ciele zapytania</param>
        /// <returns></returns>
        public String REQUEST_Location(String method, String url, Dictionary<String, String> getData, Dictionary<String, String> postData, Dictionary<String, String> headers, byte[] rawData)
        {
            String funcName = "REQUEST";

            //url = "http://httpbin.org/post"; // adres do testow

            this.log.Debug(funcName + "_Location: method: " + method);
            this.log.Debug(funcName + "_Location: url: " + url);

            // tworzenie getString z danych przekazywanymi w URL

            String getString = "";

            if (getData != null)
            {
                foreach (String key in getData.Keys)
                {
                    if (getString.Length > 0)
                        getString += "&";

                    getString +=
                        String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(getData[key]));
                }
            }

            if (getString != "")
            {
                url += "?" + getString;
            }

            this.log.Debug(funcName + "_Location: getString: " + getString);
            this.log.Debug(funcName + "_Location: url+getString: " + url);

            // tworzenie postString z danych przekazywanymi w BODY

            String postString = "";

            if (postData != null)
            {
                foreach (String key in postData.Keys)
                {
                    if (postString.Length > 0)
                        postString += "&";

                    postString +=
                        String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(postData[key]));
                }
            }

            this.log.Debug(funcName + "_Location: postString: " + postString);

            byte[] byteArray = null;

            if (postString.Length > 0)
            {
                byteArray = Encoding.UTF8.GetBytes(postString);
            }
            else if (rawData != null)
            {
                byteArray = rawData;
            }

            try
            {
                // tworzenie zapytania POST z danymi 
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;

                // dodawanie nagłówków 
                if (headers != null)
                {
                    foreach (String key in headers.Keys)
                    {
                        if (key == "Content-Type")
                        {
                            request.ContentType = headers[key];
                        }
                        else
                        {
                            request.Headers.Set(key, headers[key]);
                        }
                        this.log.Debug(funcName + "_Location: header: " + key + " = " + headers[key]);
                    }
                }

                Stream dataStream;

                if (byteArray != null)
                {
                    this.log.Debug(funcName + "_Location: rawData.Length: " + byteArray.Length);

                    // dodawanie danych do BODY

                    request.ContentLength = byteArray.Length;

                    // pobranie dostepu do strumienia
                    dataStream = request.GetRequestStream();

                    // wyslanie wszystkich danych do strumienia
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    // zamkniecie strumienia
                    dataStream.Close();
                }

                // pobieranie odpowiedzi
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                this.log.Debug(funcName + "_Location: response.StatusCode: " + response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.Headers["Location"];
                }
                else
                {
                    Dictionary<String, String> result = new Dictionary<String, String>();

                    result.Add("error", "true");
                    result.Add("path", "REQUEST_Location");
                    result.Add("response.StatusCode", response.StatusCode.ToString());
                    result.Add("method", method);
                    result.Add("url", url);
                    result.Add("getString", getString);
                    result.Add("postString", postString);
                    result.Add("headers", headers.ToString());
                    result.Add("rawData", rawData.ToString());


                    // zwrocenie bledu
                    return JsonConvert.SerializeObject(result);
                }
            }
            catch (WebException ex)
            {
                this.log.Debug(funcName + "_Location: Exception.Message: " + ex.Message);

                // proba pobrania odpowiedzi
                HttpWebResponse errorResponse = (HttpWebResponse)ex.Response;

                // proba pobrania statusu
                String errorStatus = (errorResponse == null ? "Null" : errorResponse.StatusCode.ToString());

                this.log.Debug(funcName + "_Location: Exception.errorStatus: " + errorStatus);

                Dictionary<String, String> result = new Dictionary<String, String>();

                result.Add("error", "true");
                result.Add("path", "REQUEST_Location");
                result.Add("errorResponse.StatusCode", errorStatus);
                result.Add("method", method);
                result.Add("url", url);
                result.Add("getString", getString);
                result.Add("postString", postString);
                result.Add("headers", headers.ToString());
                result.Add("rawData", rawData.ToString());


                // zwrocenie bledu
                return JsonConvert.SerializeObject(result);
            }
        }

        //---------------------------------------------------------------------------------------

        /// <summary>
        /// Pobieranie listy komend nie wymagającyh argumentów przy wywołaniu
        /// </summary>
        /// <returns>lista koemnd w postaci adresu URL</returns>
        public virtual String[] GetCommands()
        {
            this.log.Info("[BASE] GetCommands()");

            String[] commands = {};

            return commands;
        }

        //---------------------------------------------------------------------------------------
        // Pobieranie adresu do strony autoryzacji dostępu aplikacji do konta użytkownika
        //---------------------------------------------------------------------------------------

        /// <summary>
        /// Pobranie adresu strony do autoryzacji przez użytkownika dostępu do jego danych
        /// </summary>
        /// <returns>adres URL</returns>
        public virtual String GetAuthorizationUrl()
        {
            this.log.Info("[BASE] GetAuthorizationUrl(): - brak -");

            return "";
        }

        /// <summary>
        /// Pobranie tokenu dostępu i odświeżenia
        /// </summary>
        /// <param name="code">kod dostarczony podczas procesu autoryzacji</param>
        /// <returns>token dostępu i odświeżenia (format JSON)</returns>
        public virtual String GetToken(String code)
        {
            this.log.Info("[BASE] GetToken(): " + code);

            return "";
        }

        /// <summary>
        /// Pogranie nowego tokenu dostępu na podstawie tokenu odświeżenia
        /// </summary>
        /// <param name="refreshToken">token odświeżenia</param>
        /// <returns>token dostepu (format JSON)</returns>
        public virtual String RefreshToken(String refreshToken)
        {
            this.log.Info("[BASE] RefreshToken(): " + refreshToken);

            return "";
        }

        //
        // funkcja wymagana przez Hubic do pobrania dostepu do OpenStackAPI
        //

        /// <summary>
        /// Pobiereanie dodatkowych tokenów na dostęp do OpenStackAPI
        /// </summary>
        /// <returns>tokeny dla OpenStackAPI</returns>
        public virtual String GetCredential()
        {
            
            this.log.Info("[BASE] GetCredential()");

            return "";
        }

        //------------------------------------------------------------------------
        // Znajdowanie ID dla elementu na koncu podanej sciezki
        //------------------------------------------------------------------------

        /// <summary>
        /// Zamiana ścieżki na odpowiedni numer  ID
        /// </summary>
        /// <param name="path">ścieżka pliku/fodleru na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns>ID pliku/katalogu</returns>         
        public virtual String GetID(String path, String parent = null)
        {
            this.log.Info("[BASE] GetID(): " + path);

            return "";
        }

        //------------------------------------------------------------------------
        // Szukanie pliku/folderu w katalogu o podanym ID
        //------------------------------------------------------------------------

        /// <summary>
        /// Szukanie pliku/katalogu o podanym ID
        /// </summary>
        /// <param name="name">szukane ID</param>
        /// <param name="parent">katalog początkowy poszukiwań</param>
        /// <returns></returns>
        public virtual String Find(String name, String parent = null)
        {
            this.log.Info("[BASE] Find(): " + name);

            return "";
        }

        //------------------------------------------------------------------------
        // FOLDERS
        //------------------------------------------------------------------------

        /// <summary>
        /// Tworzenie nowego katalogu
        /// </summary>
        /// <param name="path">pełna ścieżka tworzonego katalogu</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns>pusty string</returns>
        public virtual String CreateFolder(String path, String parent = null)
        {
            this.log.Info("[BASE] CreateFolder: " + path);

            return "";
        }

        /// <summary>
        /// Zmiana nazwy katalogu
        /// </summary>
        /// <param name="oldPath">pełna ścieżka starego katalogu</param>
        /// <param name="newPath">pełna ścieżka nowego katalogu</param>
        /// <param name="newName">sama nazwa nowego katalogu</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns>pusty string</returns>
        public virtual String RenameFolder(String oldPath, String newPath, String newName, String parent = null)
        {
            this.log.Info("[BASE] RenameFolder: " + oldPath + " -> " + newPath + " (" + newName + ")" );

            return "";
        }

        /// <summary>
        /// Kasowanie folderu
        /// </summary>
        /// <param name="path">pełna ścieżna kasowanego katalogu</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns>pusty string</returns>
        public virtual String DeleteFolder(String path, String parent = null)
        {
            this.log.Info("[BASE] DeleteFolder: " + path);

            return "";
        }

        //--- ??? ---

        /// <summary>
        /// Przenoszenie folderu 
        /// </summary>
        /// <param name="oldPath">stara pełna ścieża</param>
        /// <param name="newPath">nowa pełna ścieża</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String MoveFolder(String oldPath, String newPath, String parent = null)
        {
            this.log.Info("[BASE] MoveFolder: " + oldPath + " -> " + newPath);

            return "";
        }

        //------------------------------------------------------------------------
        // FILES
        //------------------------------------------------------------------------

        /// <summary>
        /// Wysyłanie pliku na dysk
        /// </summary>
        /// <param name="filename">lokalna pełna ścieżka do pliku</param>
        /// <param name="remoteFilename">nazwa (ścieżka) na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String UploadFile(String filename, String remoteFilename, String parent = null)
        {
            this.log.Info("[BASE] UploadFile: " + filename);

            return "";
        }

        /// <summary>
        /// Wysyłanie pliku na dysk
        /// </summary>
        /// <param name="filename">lokalna pełna ścieżka do pliku</param>
        /// <param name="remoteFilename">nazwa (ścieżka) na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String UploadFile2(String filename, String remoteFilename, String parent = null)
        {
            this.log.Info("[BASE] UploadFile: " + filename);

            return "";
        }

        /// <summary>
        /// Zmiana nazwy pliku
        /// </summary>
        /// <param name="oldPath">stara pełna ścieżka</param>
        /// <param name="newPath">nowa pełna ścieżka</param>
        /// <param name="newName">nowa sama nazwa pliku </param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String RenameFile(String oldPath, String newPath, String newName, String parent = null)
        {
            this.log.Info("[BASE] RenameFile: " + oldPath + " -> " + newPath + " (" + newName + ")");

            return "";
        }

        /// <summary>
        /// Kasowanie pliku
        /// </summary>
        /// <param name="path">pełna ścieżka na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String DeleteFile(String path, String parent = null)
        {
            this.log.Info("[BASE] DeleteFile: " + path);

            return "";
        }

        /// <summary>
        /// Pobieranie pliku z dysku
        /// </summary>
        /// <param name="remotePath">zdalna pełna ścieżka</param>
        /// <param name="localPath">lokalna pełna ścieżka</param>
        /// <returns></returns>
        public virtual String DownloadFile(String remotePath, String localPath = "tmp")
        {
            this.log.Info("[BASE] DownloadFile: " + remotePath + " => " + localPath);

            return "";
        }

        //--- ??? ---

        /// <summary>
        /// Tworzenie pustego pliku
        /// </summary>
        /// <param name="path">pełna ścieżka z nazwą pliku</param>
        /// <returns></returns>
        public virtual String CreateFile(String path)
        {
            this.log.Info("[BASE] CreateFile: " + path);

            return "";
        }

        /// <summary>
        /// Przenoszenie pliku
        /// </summary>
        /// <param name="oldPath">stara pełna ścieżka</param>
        /// <param name="newPath">nowa pełna ścieżka</param>
        /// <returns></returns>
        public virtual String MoveFile(String oldPath, String newPath)
        {
            this.log.Info("[BASE] MoveFile: " + oldPath + " -> " + newPath);

            return "";
        }

        //------------------------------------------------------------------------
        // INNE
        //------------------------------------------------------------------------

        /// <summary>
        /// Listowanie zawartości katalogu
        /// </summary>
        /// <param name="name">pełna ścieżka</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String ListFolder(String name, String parent = null)
        {
            this.log.Info("[BASE] ListFolder: " + name);

            return "";
        }

        /// <summary>
        /// Szukanie pliku/plików
        /// </summary>
        /// <param name="name">(częściowa) nazwa pliku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public virtual String FindFiles(String name, String parent = null)
        {
            this.log.Info("[BASE] FindFiles: " + name);

            return "";
        }

        /// <summary>
        /// Listowanie (wszystkich) plików 
        /// </summary>
        /// <returns></returns>
        public virtual String ListFiles()
        {
            this.log.Info("[BASE] ListFolders: " + name);

            return "";
        }

        //------------------------------------------------------------------------
    }
}
