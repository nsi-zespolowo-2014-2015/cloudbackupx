﻿using System;
using System.Collections.Generic; // List
using System.Net;  // WebRequest, WebResponse
using System.Text; // Encoding
using System.IO;   // Stream


using Newtonsoft.Json; // JsonConvert.DeserializeObject
using Newtonsoft.Json.Linq;

namespace CloudBackup
{
    /// <summary>
    /// Klasa odpowiedzialna za komunikację z dyskiem Google
    /// </summary>
    public class CBDriverGoogle : CBDriver
    {
        public CBDriverGoogle(CBConfigDriver config = null)
        {
            this.name = "Google";
            this.log = new CBLog("CBDriver" + name);

            this.config = config;

            this.authUrl = "https://accounts.google.com/o/oauth2/auth";
            this.tokenUrl = "https://www.googleapis.com/oauth2/v3/token";
            this.commandUrl = "https://www.googleapis.com";

            this.log.Debug("constructor()");

            // obejscie problemu z nieporawnymi certyfikatami
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            //this.config = config;
            //GetConfig();

            if (config != null && config.ExpireTime < DateTime.Now)
            {
                RefreshToken(config.RefreshToken);
            }
        }

        //---------------------------------------------------------------------------------------


        /// <summary>
        /// Pobieranie listy komend nie wymagającyh argumentów przy wywołaniu
        /// </summary>
        /// <returns>lista koemnd w postaci adresu URL</returns>
        public override String[] GetCommands()
        {
            String[] commands = {
                "/drive/v2/about",
                "/drive/v2/changes",
                "/drive/v2/files"
            };

            return commands;
        }

        //---------------------------------------------------------------------------------------
        // Pobieranie adresu do strony autoryzacji dostępu aplikacji do konta użytkownika
        //---------------------------------------------------------------------------------------

        /// <summary>
        /// Pobranie adresu strony do autoryzacji przez użytkownika dostępu do jego danych
        /// </summary>
        /// <returns>adres URL</returns>
        public override String GetAuthorizationUrl()
        {
            String funcName = "GetAuthorizationUrl()";

            this.log.Debug(funcName + ": START");

            List<String> data = new List<String>();
            data.Add("client_id=" + config.ClientID);
            data.Add("response_type=" + "code");
            data.Add("redirect_uri=" + config.RedirectURL); //"http://localhost/");
            data.Add("state=" + "moj_state");
            data.Add("scope=" + "https://www.googleapis.com/auth/drive");

            String url = authUrl + "?" + string.Join("&", data.ToArray());
            this.log.Debug("url: " + url);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                this.log.Debug(funcName + ": StatusCode: " + response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    this.log.Debug(funcName + ": ResponseUri: " + response.ResponseUri);
                    return response.ResponseUri.OriginalString;//.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (WebException ex)
            {
                this.log.Debug(funcName + ": Exception: Message: " + ex.Message);

                HttpWebResponse errorResponse = (HttpWebResponse)ex.Response;

                String errorStatus = (errorResponse == null ? "NULL" : errorResponse.StatusCode.ToString());

                this.log.Debug(funcName + ": Exception: StatusCode: " + errorStatus);

                return "";
            }
        }

        /// <summary>
        /// Pobranie tokenu dostępu i odświeżenia
        /// </summary>
        /// <param name="code">kod dostarczony podczas procesu autoryzacji</param>
        /// <returns>token dostępu i odświeżenia (format JSON)</returns>
        public override String GetToken(String code)
        {
            this.log.Debug("GetToken(): [BEGIN]");
            this.log.Debug("GetToken(): code: " + code);

            Dictionary<String, String> postData = new Dictionary<String, String>();
            postData.Add("client_id", config.ClientID);
            postData.Add("client_secret", config.ClientSecret);
            postData.Add("code", code);
            postData.Add("grant_type", "authorization_code");
            postData.Add("redirect_uri", config.RedirectURL);
            postData.Add("scope", "");

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");

            //String result = POST(tokenUrl, null, postData, null, null);
            String result = REQUEST("POST", tokenUrl, null, postData, headers, null);

            this.log.Debug("GetToken(): result: " + result);
            this.log.Debug("GetToken(): [END]");

            //----------
            if (result != null && result != "")
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                if (json.GetValue("access_token") != null && json.GetValue("access_token").ToString() != "")
                {
                    config.AccessToken = json.GetValue("access_token").ToString();
                    config.AccessTokenType = json.GetValue("token_type").ToString();
                    config.AccessTokenExpire = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).ToString();
                    config.RefreshToken = json.GetValue("refresh_token").ToString();

                    config.Save();
                }
            }
            //-------

            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        /// <summary>
        /// Pogranie nowego tokenu dostępu na podstawie tokenu odświeżenia
        /// </summary>
        /// <param name="refreshToken">token odświeżenia</param>
        /// <returns>token dostepu (format JSON)</returns>
        public override String RefreshToken(String refreshToken)
        {
            this.log.Debug("RefreshToken(): [BEGIN]");
            this.log.Debug("RefreshToken(): refreshToken: " + refreshToken);

            Dictionary<String, String> postData = new Dictionary<String, String>();
            postData.Add("client_id", config.ClientID);
            postData.Add("client_secret", config.ClientSecret);
            postData.Add("grant_type", "refresh_token");
            postData.Add("refresh_token", config.RefreshToken);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");

            //String result = POST(tokenUrl, null, postData, headers, null);
            String result = REQUEST("POST", tokenUrl, null, postData, headers, null);

            this.log.Debug("RefreshToken(): result: " + result);
            this.log.Debug("RefreshToken(): [END]");

            //----------
            if (result != null && result != "")
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                if (json.GetValue("access_token") != null && json.GetValue("access_token").ToString() != "")
                {
                    config.AccessToken = json.GetValue("access_token").ToString();
                    config.AccessTokenType = json.GetValue("token_type").ToString();
                    config.AccessTokenExpire = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).Ticks.ToString();
                    config.ExpireTime = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString()));

                    config.Save();
                }
            }

            //-------
            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        //------------------------------------------------------------------------
        // Znajdowanie ID dla elementu na koncu podanej sciezki
        //------------------------------------------------------------------------

        /// <summary>
        /// Zamiana ścieżki na odpowiedni numer  ID
        /// </summary>
        /// <param name="path">ścieżka pliku/fodleru na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns>ID pliku/katalogu</returns>  
        public override String GetID(String path, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            //this.log.Debug("CBDriverGoogle.GetID: path: " + path);
            String[] parts = path.Split(@"\/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (String element in parts)
            {
                String result = Find(element, parentID);
                //this.log.Debug("CBDriverGoogle.GetID: Find.id: " + id);

                dynamic json = JsonConvert.DeserializeObject(result);

                //this.log.Debug("CBDriverGoogle.GetID: json[\"error\"]: " + json["error"]);
                //this.log.Debug("CBDriverGoogle.GetID: json[\"items\"]: " + json["items"]);
                //this.log.Debug("CBDriverGoogle.GetID: json[\"items\"].Count: " + json["items"].Count);

                if (json["error"] != null || json["items"] == null || json["items"].Count == 0)
                {
                    return "";
                }
                else
                {
                    //this.log.Debug("GetID: path: " + element);
                    //this.log.Debug("GetID: parentID: " + parentID);
                    parentID = json["items"][0]["id"];
                    //this.log.Debug("GetID: id: " + parentID);
                }
            }

            //this.log.Debug("CBDriverGoogle.GetID: parentID: " + parentID);
            return parentID;
        }

        //------------------------------------------------------------------------
        // Szukanie pliku/folderu w katalogu o podanym ID
        //------------------------------------------------------------------------

        /// <summary>
        /// Szukanie pliku/katalogu o podanym ID
        /// </summary>
        /// <param name="name">szukane ID</param>
        /// <param name="parent">katalog początkowy poszukiwań</param>
        /// <returns></returns>
        public override String Find(String name, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            //this.log.Debug("Find: path: " + path);
            //this.log.Debug("Find: parentID: " + parentID);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("q", "title='" + name + "' and '" + parentID + "' in parents and trashed=false");

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", getData, null, headers, null);

            //this.log.Debug("Find: result: " + result);
            return result;
        }

        //------------------------------------------------------------------------
        // FOLDERS
        //------------------------------------------------------------------------

        /// <summary>
        /// Tworzenie nowego katalogu
        /// </summary>
        /// <param name="path">pełna ścieżka tworzonego katalogu</param>
        /// <param name="parentID">ID katalogu początkowego (domyślnie: root)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String CreateFolder(String path, String parentID = "root")
        {
            String funcName = "CreateFolder()";

            if (parentID == null) parentID = "root";

            this.log.Debug(funcName + ": path: " + path);
            this.log.Debug(funcName + ": parent: " + parentID);

            String[] parts = path.Split(@"\/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            this.log.Debug(funcName + ": parts: " + parts.Length);
            foreach (String element in parts)
            {
                this.log.Debug(funcName + ": [parts]: " + element);
            }

            String result = "";

            foreach (String element in parts)
            {
                this.log.Debug(funcName + ": element: " + element);

                String id = GetID(element, parentID);

                this.log.Debug(funcName + ": GetID: " + id);

                if (id == "")
                {
                    Dictionary<String, String> headers = new Dictionary<String, String>();
                    headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
                    headers.Add("Content-Type", "application/json");

                    String jsonString =
                        "{" +
                            "\"title\":\"" + element + "\"," +
                            "\"mimeType\":\"application/vnd.google-apps.folder\"," +
                            "\"parents\":[{\"id\":\"" + parentID + "\"}]" +
                        "}";

                    byte[] rawData = Encoding.UTF8.GetBytes(jsonString);

                    //this.log.Debug(funcName + ": jsonString: " + rawData.Length + " " + Encoding.UTF8.GetString(rawData));

                    result = REQUEST("POST", commandUrl + "/drive/v2/files", null, null, headers, rawData);

                    //this.log.Debug(funcName + ": POST.result : " + result);

                    JObject json = JsonConvert.DeserializeObject<JObject>(result);

                    if (json.GetValue("error") != null || json.GetValue("id") == null)
                    {
                        this.log.Debug(funcName + ": json: error");
                        return result;
                    }
                    else
                    {
                        parentID = json.GetValue("id").ToString();
                        result = parentID;
                    }
                }
                else
                {
                    parentID = id;
                    result = parentID;
                }
            }

            this.log.Debug(funcName + ": result: " + result);

            return result;
        }

        /// <summary>
        /// Zmiana nazwy katalogu
        /// </summary>
        /// <param name="oldPath">pełna ścieżka starego katalogu</param>
        /// <param name="newPath">pełna ścieżka nowego katalogu</param>
        /// <param name="newName">sama nazwa nowego katalogu</param>
        /// <param name="parentID">ID katalogu początkowego (domyślnie: root)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String RenameFolder(String oldPath, String newPath, String newName, String parentID = "root")
        {
            String funcName = "CreateFolder()";

            if (parentID == null) parentID = "root";

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parentID);

            // zamiana nazwy katalogu na ID
            String id = GetID(oldPath, parentID);

            //this.log.Debug(funcName + ": GetID: " + id);

            if (id == "")
            {
                return "{\"error\":\"true\",\"message\":\"no ID for " + oldPath + "\"}";
            }

            //TODO: sprawdzic czy znaleziony element to folder

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
            headers.Add("Content-Type", "application/json");

            String jsonString =
                "{" +
                    "\"title\":\"" + newName + "\"" + //," +
                //"\"mimeType\":\"application/vnd.google-apps.folder\"," +
                "}";

            byte[] rawData = Encoding.UTF8.GetBytes(jsonString);

            String result = REQUEST("PUT", commandUrl + "/drive/v2/files/" + id, null, null, headers, rawData);

            //this.log.Debug(funcName + ": PUT.result : " + result);

            if (result == "")
            {
                return "{\"ok\":\"true\",\"message\":\"renamed " + oldPath + " to " + newPath + "\"}";
            }

            this.log.Debug(funcName + ": result: " + result);

            return result; //"{\"error\":\"true\"}"
        }

        /// <summary>
        /// Kasowanie folderu
        /// </summary>
        /// <param name="path">pełna ścieżna kasowanego katalogu</param>
        /// <param name="parentID">ID katalogu początkowego (domyślnie: root)</param>
        /// <returns>JSON zwracany przez driver.REQUEST()</returns>
        public override String DeleteFolder(String path, String parentID = "root")
        {
            String funcName = "DeleteFolder()";

            if (parentID == null) parentID = "root";

            this.log.Debug(funcName + ": path: " + path);
            this.log.Debug(funcName + ": parent: " + parentID);

            String id = GetID(path, parentID);

            //this.log.Debug("CBDriverGoogle.DeleteFolder: GetID: " + id);

            String result = "";

            if (id == "")
            {
                result = "{\"error\":\"true\",\"message\":\"no ID for " + path + "\"}";
            }
            else
            {
                //TODO: sprawdzic czy znaleziony element to folder

                Dictionary<String, String> headers = new Dictionary<String, String>();
                headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

                result = REQUEST("DELETE", commandUrl + "/drive/v2/files/" + id, null, null, headers, null);

                //this.log.Debug("CBDriverGoogle.DeleteFolder: DELETE.result : " + result);

                if (result == "")
                {
                    result = "{\"ok\":\"true\",\"message\":\"deleted " + path + "\"}";
                }
            }

            this.log.Debug(funcName + ": result: " + result);

            return result; //"{\"error\":\"true\"}"
        }

        //------------------------------------------------------------------------
        // FILES
        //------------------------------------------------------------------------

        /// <summary>
        /// Wysyłanie pliku na dysk
        /// </summary>
        /// <param name="filename">lokalna pełna ścieżka do pliku</param>
        /// <param name="remoteFilename">nazwa (ścieżka) na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String UploadFile(String filename, String remoteFilename, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            String id = GetID(remoteFilename, parentID);

            byte[] rawData;

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("uploadType", "media");

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
            headers.Add("Content-Type", "application/octet-stream");

            try
            {
                rawData = File.ReadAllBytes(filename);
            }
            catch (Exception ex)
            {
                Dictionary<String, String> jsonData = new Dictionary<String, String>();
                jsonData.Add("error", "true");
                jsonData.Add("path", "CBDriverGoogle.UploadFile()");
                jsonData.Add("message", ex.Message);
                jsonData.Add("StatusCode", "0");

                // zwrocenie bledu
                return JsonConvert.SerializeObject(jsonData);
            }

            //String result = POST(commandUrl + "/upload/drive/v2/files", query, null, headers, data);
            String result = REQUEST("POST", commandUrl + "/upload/drive/v2/files", getData, null, headers, rawData);

            //TODO: nadanie nazwy zewnętrznej

            return result;
        }

        /// <summary>
        /// Wysyłanie pliku na dysk
        /// </summary>
        /// <param name="filename">lokalna pełna ścieżka do pliku</param>
        /// <param name="remoteFilename">nazwa (ścieżka) na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String UploadFile2(String filename, String remoteFilename, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            this.log.Debug("CBDriverGoogle.UploadFile2: [BEGIN]");

            String id = "root";

            int idx = remoteFilename.LastIndexOf("/", System.StringComparison.Ordinal);
            String path = remoteFilename.Substring(0, idx);
            String name = remoteFilename.Substring(idx + 1);

            if( path.Length > 0 )
            {
                id = CreateFolder(path);
                remoteFilename = name;
            }

            Dictionary<String, String> getData;
            Dictionary<String, String> headers;
            byte[] rawData;

            String result = "";

            //--- nazwa pliku ---

            getData = new Dictionary<String, String>();
            getData.Add("uploadType", "resumable");

            headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
            headers.Add("Content-Type", "application/json");
            //headers.Add("X-Upload-Content-Type", "application/octet-stream");
            //headers.Add("X-Upload-Content-Lengt", "2000000");

            String jsonString =
                "{" +
                  "\"title\":\"" + remoteFilename + "\"," +
                  "\"parents\": [{\"id\":\"" + id + "\"}]" +
                "}";

            rawData = Encoding.UTF8.GetBytes(jsonString);
            this.log.Debug("CBDriverGoogle.UploadFile2: jsonString: " + jsonString);

            String location = REQUEST_Location("POST", commandUrl + "/upload/drive/v2/files/", getData, null, headers, rawData);
            this.log.Debug("CBDriverGoogle.UploadFile2: location: " + location);

            if (!location.StartsWith("{"))
            {
                //--- zawartosc pliku ---

                headers = new Dictionary<String, String>();
                headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
                headers.Add("Content-Type", "application/octet-stream");
                //headers.Add("Content-Length", "2000000");
                try
                {
                    rawData = File.ReadAllBytes(filename);
                    //headers.Add("Content-Length", rawData.Length.ToString());
                }
                catch (Exception ex)
                {
                    Dictionary<String, String> jsonData = new Dictionary<String, String>();
                    jsonData.Add("error", "true");
                    jsonData.Add("path", "CBDriverGoogle.UploadFile()");
                    jsonData.Add("message", ex.Message);
                    jsonData.Add("StatusCode", "0");

                    // zwrocenie bledu
                    return JsonConvert.SerializeObject(jsonData);
                }

                result = REQUEST("PUT", location, null, null, headers, rawData);

                //TODO: nadanie nazwy zewnętrznej
            }

            return result;
        }

        /// <summary>
        /// Zmiana nazwy pliku
        /// </summary>
        /// <param name="oldPath">stara pełna ścieżka</param>
        /// <param name="newPath">nowa pełna ścieżka</param>
        /// <param name="newName">nowa sama nazwa pliku </param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String RenameFile(String oldPath, String newPath, String newName, String parentID = "root")
        {
            String funcName = "RenameFile()";

            if (parentID == null) parentID = "root";

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parentID);

            String id = GetID(oldPath, parentID);

            //this.log.Debug(funcName + ": GetID: " + id);

            String result = "";

            if (id == "")
            {
                result = "{\"error\":\"true\",\"message\":\"no ID for " + oldPath + "\"}";
            }
            else
            {
                //TODO: sprawdzic czy znaleziony element to folder

                Dictionary<String, String> headers = new Dictionary<String, String>();
                headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
                headers.Add("Content-Type", "application/json");

                String jsonString =
                    "{" +
                        "\"title\":\"" + newName + "\"" +
                    "}";

                byte[] rawData = Encoding.UTF8.GetBytes(jsonString);

                result = REQUEST("PUT", commandUrl + "/drive/v2/files/" + id, null, null, headers, rawData);

                //this.log.Debug(funcName + ": PUT.result : " + result);

                if (result == "")
                {
                    //return "{\"ok\":\"true\",\"message\":\"renamed " + oldPath + " to " + newPath + "\"}";
                    result = "{\"error\":\"true\"}";
                }
            }

            this.log.Debug(funcName + ": result: " + result);

            return result; //"{\"error\":\"true\"}"
        }

        /// <summary>
        /// Kasowanie pliku
        /// </summary>
        /// <param name="path">pełna ścieżka na dysku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String DeleteFile(String name, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            String id = GetID(name, parentID);

            //this.log.Debug("CBDriverGoogle.DeleteFile: GetID: " + id);

            if (id == "")
            {
                return "{\"error\":\"true\",\"message\":\"no ID for " + name + "\"}";
            }

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("DELETE", commandUrl + "/drive/v2/files/" + id, null, null, headers, null);

            return result;
        }

        /// <summary>
        /// Pobieranie pliku z dysku
        /// </summary>
        /// <param name="remotePath">zdalna pełna ścieżka</param>
        /// <param name="localPath">lokalna pełna ścieżka</param>
        /// <returns></returns>
        public override String DownloadFile(String id, String filename = "tmp")
        {
            //TODO: niezrobione

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/drive/v2/files/" + id, null, null, headers, null);

            return result;
        }

        //------------------------------------------------------------------------
        // INNE
        //------------------------------------------------------------------------

        /// <summary>
        /// Listowanie zawartości katalogu
        /// </summary>
        /// <param name="name">pełna ścieżka</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String ListFolder(String name, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            if (name != "")
            {
                String id = GetID(name, parentID);

                if (id == "")
                {
                    return "{\"error\":\"true\",\"message\":\"no ID for " + name + "\"}";
                }

                parentID = id;
            }
            //this.log.Debug("CBDriverGoogle.DeleteFolder: GetID: " + id);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("q", "mimeType='application/vnd.google-apps.folder' and '" + parentID + "' in parents and trashed=false");

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", getData, null, headers, null);

            return result;
        }

        /// <summary>
        /// Szukanie pliku/plików
        /// </summary>
        /// <param name="name">(częściowa) nazwa pliku</param>
        /// <param name="parent">katalog początkowy</param>
        /// <returns></returns>
        public override String FindFiles(String name, String parentID = "root")
        {
            if (parentID == null) parentID = "root";

            String[] elements = name.Split("/".ToCharArray());

            foreach (String i in elements)
            {
                String r = Find(i, parentID);

                dynamic json = JsonConvert.DeserializeObject(r);

                if (json["items"] == null || json["items"].Count == 0)
                {
                    return "";
                }

                parentID = json["items"][0]["id"];

                this.log.Debug("FindFiles: path: " + i);
                this.log.Debug("FindFiles: parentID: " + parentID);
                this.log.Debug("FindFiles: id: " + json["items"][0]["id"]);
            }

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            //getData.Add("mimeType='application/vnd.google-apps.folder'");
            //getData.Add("q", "title contains '" + title + "'");
            getData.Add("q", "title='" + name + "' and '" + parentID + "' in parents");

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", getData, null, headers, null);
            //String result = GET("http://httpbin.org/get", getData, null, headers, null);

            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        /// <summary>
        /// Listowanie (wszystkich) plików 
        /// </summary>
        /// <returns></returns>
        public override String ListFiles()
        {
            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", null, null, headers, null);

            //return JsonConvert.DeserializeObject(result);
            return result;
        }
    }
}
