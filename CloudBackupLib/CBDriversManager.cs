﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration; // ConfigurationManager
using System.Collections.Specialized;
using System.Collections;

// http://stackoverflow.com/questions/14963870/how-to-get-all-sections-by-path-in-the-sectiongroup-applicationsettings-in-net

namespace CloudBackup
{
    /// <summary>
    /// Zarządzanie driverami
    /// </summary>
    public class CBDriversManager
    {
        CBConfig config;

        public CBDriversManager()
        {
            config = new CBConfig();
        }

        /// <summary>
        /// Pobieranie nazw dostępnych driverów
        /// </summary>
        /// <returns></returns>
        public List<String> GetDriversNames()
        {
            return config.GetDriversNames();
        }

        /// <summary>
        /// Pobieranie konfiguracji dla podanego drivera
        /// </summary>
        /// <param name="driverName">nazwa drivera</param>
        /// <returns></returns>
        public CBConfigDriver GetDriverConfig(String driverName)
        {
            return config.GetDriverConfig(driverName);
        }
    }
}
