﻿using System;
using System.IO;
using System.Diagnostics;
using System.Security.Permissions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CloudBackup
{
    /// <summary>
    /// Nazsłuchiwanie zmian w systemie plików
    /// </summary>
    class CBWatcher
    {
        CBLog log;

        CBConfigDriver DriverConfig;
        CBConfigFolder FolderConfig;

        CBDriver driver;
        FileSystemWatcher watcher;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="folderConfig">konfiguracja folderu</param>
        /// <param name="driverConfig">konfiguracaj drivera</param>
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public CBWatcher(CBConfigFolder folderConfig, CBConfigDriver driverConfig)
        {
            String funcName = "Constructor()";

            this.DriverConfig = driverConfig;
            this.FolderConfig = folderConfig;

            this.log = new CBLog("CBWatcher [" + FolderConfig.SrcFolderName + " => " + FolderConfig.DriverName + ":" + FolderConfig.DstFolderName + "]");

            this.log.Info(funcName + ": [BEGIN]");

            //-------------------------------------

            CBSender sender = new CBSender();

            this.driver = sender.GetDriver(FolderConfig.DriverName);
            this.driver.SetConfig(DriverConfig);

            //-------------------------------------

            if ( this.DriverConfig.ExpireTime <= DateTime.Now)
            {
                this.driver.RefreshToken(this.DriverConfig.RefreshToken);
            }
            this.driver.RefreshToken(this.DriverConfig.RefreshToken);

            //-------------------------------------

            watcher = new FileSystemWatcher();

            watcher.Path = FolderConfig.SrcFolderName;

            watcher.NotifyFilter = 
                //NotifyFilters.LastAccess | 
                //NotifyFilters.Size |
                NotifyFilters.LastWrite | 
                NotifyFilters.FileName | 
                NotifyFilters.DirectoryName;
            
            watcher.Filter = "*.*"; // domyślne ustawienie

            // dodanie uchwytów oczekiwania

            watcher.Created += new FileSystemEventHandler(this.OnCreated);
            watcher.Changed += new FileSystemEventHandler(this.OnChanged);
            watcher.Deleted += new FileSystemEventHandler(this.OnDelete);
            watcher.Renamed += new RenamedEventHandler(this.OnRenamed);

            watcher.IncludeSubdirectories = true;

            // włączenie nasłuchiwania na zdarzenia
            // (bez tego możliwe jest wyłącznie nasłuchiwanie
            // w trybie synchronicznym)
            watcher.EnableRaisingEvents = true;

            //-------------------------------------

            this.log.Info(funcName + ": [END]");
        }

        /// <summary>
        /// Niewykorzystywana
        /// </summary>
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void Run(){}

        /// <summary>
        /// Starowanie watchera przez usługę
        /// </summary>
        public void Start()
        {
            this.log.Info("Start()");

            this.driver.Start();
        }

        /// <summary>
        /// Zatrzymywanie watchera przez usługę
        /// </summary>
        public void Stop()
        {
            this.driver.Stop();

            this.log.Info("Stop()");
        }

        // definiowanie metod obsługi zdarzeń

        /// <summary>
        /// Obsługa zdarzeń OnCreated - tworzenie pliku/folderu
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnCreated(object source, FileSystemEventArgs e)
        {
            String funcName = "OnCreated()";

            this.log.Info(funcName + ": e.FullPath: " + e.FullPath);
            this.log.Info(funcName + ": e.Name    : " + e.Name);


            if (this.DriverConfig.ExpireTime <= DateTime.Now)
            {
                this.driver.RefreshToken(this.DriverConfig.RefreshToken);
            }

            String remoteFullPath = this.FolderConfig.DstFolderName + "/" + e.Name.Replace("\\", "/");

            FileAttributes attr = File.GetAttributes(e.FullPath);

            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                this.log.Info("Utworzono Folde:\n   LOCAL: " + e.Name + "\n  REMOTE: " + remoteFullPath);
                this.log.Info("driver.CreateFolder()");
                this.driver.CreateFolder(remoteFullPath);
                String[] files = Directory.GetFiles(e.FullPath, "*.*", SearchOption.AllDirectories);

                Parallel.ForEach(files, file => 
                {
                    remoteFullPath = file.Replace(this.FolderConfig.SrcFolderName, this.FolderConfig.DstFolderName).Replace("\\", "/");
                    this.log.Info("TODO: driver.OnCreated()???: " + remoteFullPath);
                    this.driver.UploadFile2(file, remoteFullPath);
                });
            }
            else
            {
                this.log.Info("Utworzono Plik:\n   LOCAL: " + e.Name + "\n  REMOTE: " + remoteFullPath);
                this.log.Info("driver.UploadFile2()");
                this.driver.UploadFile2(e.FullPath, remoteFullPath);
            }
        }

        /// <summary>
        /// Obsługa zdarzeń OnChanged - modyfikacja zawartości pliku/folderu
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            String funcName = "OnChanged()";

            this.log.Info(funcName + ": e.FullPath: " + e.FullPath);
            this.log.Info(funcName + ": e.Name    : " + e.Name);

            if (this.DriverConfig.ExpireTime <= DateTime.Now)
            {
                this.driver.RefreshToken(this.DriverConfig.RefreshToken);
            }

            String remoteFullPath = this.FolderConfig.DstFolderName + "/" + e.Name.Replace("\\", "/");

            if (File.Exists(e.FullPath))
            {
                FileAttributes attr = File.GetAttributes(e.FullPath);

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    this.log.Info("Zmodyfikowano Folder:\n   LOCAL: " + e.FullPath + "\n  REMOTE: " + remoteFullPath);
                    this.log.Info("TODO: driver.OnChange()???");
                    String[] files = Directory.GetFiles(e.FullPath, "*.*", SearchOption.AllDirectories);

                    //foreach (String file in files)
                    //{
                    //    remoteFullPath = file.Replace(this.FolderConfig.SrcFolderName, this.FolderConfig.DstFolderName).Replace("\\", "/");
                    //    this.log.Info("TODO: driver.OnChange()???: " + remoteFullPath);
                    //    this.driver.UploadFile2(file, remoteFullPath);
                    //}

                    Parallel.ForEach(files, file =>
                    {
                        remoteFullPath = file.Replace(this.FolderConfig.SrcFolderName, this.FolderConfig.DstFolderName).Replace("\\", "/");
                        this.log.Info("TODO: driver.OnCreated()???: " + remoteFullPath);
                        this.driver.UploadFile2(file, remoteFullPath);
                    });
                }
                else
                {
                    this.log.Info("Zmodyfikowano Plik:\n   LOCAL: " + e.FullPath + "\n  REMOTE: " + remoteFullPath);
                    this.log.Info("driver.UploadFile2()");
                    this.driver.UploadFile2(e.FullPath, remoteFullPath);
                }
            }
        }

        /// <summary>
        /// Obsługa zdarzeń OnDelete - kasowanie pliku/folderu (bezrekurencyjnie)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnDelete(object source, FileSystemEventArgs e)
        {
            String funcName = "OnDelete()";

            this.log.Info(funcName + ": e.FullPath: " + e.FullPath);
            this.log.Info(funcName + ": e.Name    : " + e.Name);
            
            if (this.DriverConfig.ExpireTime <= DateTime.Now)
            {
                this.driver.RefreshToken(this.DriverConfig.RefreshToken);
            }

            String remoteFullPath = this.FolderConfig.DstFolderName + "/" + e.Name.Replace("\\", "/");

            // nie sposob okreslic atrybutow skasowanego pliku :)
            //FileAttributes attr = File.GetAttributes(e.FullPath);

            //if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            //{
            //    this.log.Info("Skasowano Folder: " + e.FullPath);
            //    this.driver.DeleteFolder(remoteFullPath);
            //}
            //else
            //{
                this.log.Info("Skasowano Plik/Folder: " + e.FullPath);
                this.log.Info("driver.DeleteFile()");
                this.driver.DeleteFile(remoteFullPath);
            //}

        }

        /// <summary>
        /// Zmiana nazwy pliku/folderu
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnRenamed(object source, RenamedEventArgs e)
        {
            String funcName = "OnRenamed()";

            this.log.Info(funcName + ": e.FullPath: " + e.FullPath);
            this.log.Info(funcName + ": e.Name    : " + e.Name);
            this.log.Info(funcName + ": e.OldFullPath: " + e.OldFullPath);
            this.log.Info(funcName + ": e.OldName    : " + e.OldName);

            if (this.DriverConfig.ExpireTime <= DateTime.Now)
            {
                this.driver.RefreshToken(this.DriverConfig.RefreshToken);
            }

            // metoda jest uruchamiana w przypadku zmiany nazwy pliku

            String remoteOldFullPath = this.FolderConfig.DstFolderName + "/" + e.OldName.Replace("\\", "/");
            String remoteNewFullPath = this.FolderConfig.DstFolderName + "/" + e.Name.Replace("\\", "/");

            String[] parts = e.Name.Split("\\".ToCharArray());
            String remoteNewName = parts[parts.Length - 1];

            FileAttributes attr = File.GetAttributes(e.FullPath);

            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                this.log.Info("Zmieniono Nazwe Folderu:\n   LOCAL: " + e.OldName + " => " + e.Name + "\n  REMOTE: " + remoteOldFullPath + " => " + remoteNewFullPath);
                this.log.Info("driver.RenameFolder()");
                this.driver.RenameFolder(remoteOldFullPath, remoteNewFullPath, remoteNewName);
            }
            else
            {
                this.log.Info("Zmieniono Nazwe Pliku:\n   LOCAL: " + e.OldName + " => " + e.Name + "\n  REMOTE: " + remoteOldFullPath + " => " + remoteNewFullPath);
                this.log.Info("driver.RenameFile()");
                this.driver.RenameFile(remoteOldFullPath, remoteNewFullPath, remoteNewName);
            }
        }
    }
}
