﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration; // ConfigurationManager
using System.Collections.Specialized;
using System.Collections;

using System.Xml.Linq;

// http://stackoverflow.com/questions/14963870/how-to-get-all-sections-by-path-in-the-sectiongroup-applicationsettings-in-net
// http://stackoverflow.com/questions/12877484/custom-config-section-in-app-config-c-sharp

namespace CloudBackup
{
    /// <summary>
    /// Klasa zarządzająca konfiguracjami
    /// </summary>
    public class CBConfig
    {
        CBLog log;

        /// <summary>
        /// XElement przechowujący całą konfigurację
        /// </summary>
        public XElement xconfig; 

        /// <summary>
        /// XElement przechowujący konfiguracje driverów
        /// </summary>
        public XElement section_drivers;

        /// <summary>
        /// XElement przechowujący konfiguracje połączeń
        /// </summary>
        public XElement section_connections;

        /// <summary>
        /// XElement przechowujący konfiguracje folderów
        /// </summary>        
        public XElement section_folders;
        
        String FileName = "";
        
        /// <summary>
        /// Obsługa pliku konfiguracyjnego
        /// </summary>
        /// <param name="FileName">nazwa pliku z konfiguracją. W razie braku wczytuje plik "cbconfig.xml" z katalogu programu</param>
        public CBConfig(String FileName="")
        {
            log = new CBLog("CBConfig");

            this.log.Debug("[BEGIN]");

            if( FileName == "" ) {
                this.FileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cbconfig.xml");
            }

            this.log.Debug("FileName: " + this.FileName);


            xconfig = XElement.Load(this.FileName);

            section_drivers = xconfig.Element("Drivers");
            section_folders = xconfig.Element("Folders");

            this.log.Debug("section Drivers:\n" + section_drivers);
            this.log.Debug("section Folders:\n" + section_folders);
        }

        /// <summary>
        /// Zapis całego pliku konfiguracyjnego
        /// </summary>
        public void Save()
        {
            this.log.Debug("Save(): section Drivers:\n" + section_drivers);
            this.log.Debug("Save(): section Folders:\n" + section_folders);
            
            xconfig.Save(this.FileName);
        }

        /// <summary>
        /// Pobieranie nazw dostępnych driverów (czyli także nazw obsługiwanych dysków chmurowych)
        /// </summary>
        /// <returns>lista nazw driverów</returns>
        public List<String> GetDriversNames()
        {
            this.log.Debug("GetDriversNames()");

            List<String> driversNames = new List<String>();

            foreach (XElement el in xconfig.Element("Drivers").Elements("add"))
            {
                this.log.Debug(" -> " + el.Attribute("Name").Value);
                driversNames.Add(el.Attribute("Name").Value);
            }

            return driversNames;
        }

        /// <summary>
        /// Pobranie konfiguracji dla podanego drivera
        /// </summary>
        /// <param name="driverName">nazwa drivera</param>
        /// <returns>obiekt z konfiguracją</returns>
        public CBConfigDriver GetDriverConfig(String driverName)
        {
            this.log.Debug(this.GetType().Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name);

            foreach (XElement element in section_drivers.Elements())
            {
                if (element.Attribute("Name").Value == driverName)
                {
                    return new CBConfigDriver(this, element);
                }
            }

            return null;
        }

        /// <summary>
        /// Pobranie konfiguracji foldera o podanej nazwie (konfiguracji)
        /// </summary>
        /// <param name="name">nazwa konfiguracji (a nie nazwa folderu)</param>
        /// <returns>obiekt z konfiguracją</returns>
        public CBConfigFolder GetFolderConfig(String name)
        {
            this.log.Debug(this.GetType().Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name);

            foreach (XElement element in section_folders.Elements())
            {
                if (element.Attribute("Name").Value == name)
                {
                    return new CBConfigFolder(this, element);
                }
            }

            return null;
        }

        /// <summary>
        /// Pobieranie wszystkich konfiguracji folderów
        /// </summary>
        /// <returns>lista obiektów z konfiguracjami</returns>
        public List<CBConfigFolder> GetAllFolderConfigs()
        {
            this.log.Debug("GetAllFolderConfigs(): section:\n" + section_folders);

            List<CBConfigFolder> result = new List<CBConfigFolder>();

            foreach (XElement element in section_folders.Elements())
            {
                result.Add(new CBConfigFolder(this, element));
            }

            this.log.Debug("GetAllFolderConfigs(): result.Count: " + result.Count);

            return result;
        }

        /// <summary>
        /// Pobieranie wszystkich konfiguracji driverów
        /// </summary>
        /// <returns>lista obiektów z konfiguracjami</returns>
        public List<CBConfigDriver> GetAllDriverConfigs()
        {
            this.log.Debug("GetAllDriverConfigs(): section:\n" + section_drivers);

            List<CBConfigDriver> result = new List<CBConfigDriver>();

            foreach (XElement element in section_folders.Elements())
            {
                result.Add(new CBConfigDriver(this, element));
            }

            this.log.Debug("GetAllDriverConfigs(): result.Count: " + result.Count);

            return result;
        }

        /// <summary>
        /// Dodanie nowej konfiguracji folderu
        /// </summary>
        /// <param name="configFolder">obiekt z konfiguracją folderu</param>
        public void AddConfigFolder(CBConfigFolder configFolder)
        {
            section_folders.Add(configFolder);
        }
    }

    /// <summary>
    /// Konfiguracja folderu
    /// </summary>
    public class CBConfigFolder
    {
        CBLog log;

        CBConfig parent;
        XElement element;

        public String Name
        {
            get {
                if (element.Attribute("Name") == null) element.SetAttributeValue("Name", "");
                return (String)element.Attribute("Name").Value;
            }
            set { element.Attribute("Name").Value = value; }
        }

        public String SrcFolderName
        {
            get {
                if (element.Attribute("SrcFolderName") == null) element.SetAttributeValue("SrcFolderName", "");
                return (String)element.Attribute("SrcFolderName").Value;
            }
            set { element.Attribute("SrcFolderName").Value = value; }
        }

        public String DstFolderName
        {
            get {
                if (element.Attribute("DstFolderName") == null) element.SetAttributeValue("DstFolderName", ""); 
                return (String)element.Attribute("DstFolderName").Value;
            }
            set { element.Attribute("DstFolderName").Value = value; }
        }

        public String DriverName
        {
            get
            {
                if (element.Attribute("DriverName") == null) element.SetAttributeValue("DriverName", "");
                return (String)element.Attribute("DriverName").Value;
            }
            set { element.Attribute("DriverName").Value = value; }
        }

        public String AccessToken
        {
            get
            {
                if (element.Attribute("AccessToken") == null) element.SetAttributeValue("AccessToken", "");
                return (String)element.Attribute("AccessToken").Value;
            }
            set { element.Attribute("AccessToken").Value = value; }
        }
        
        public String AccessTokenType
        {
            get
            {
                if (element.Attribute("AccessTokenType") == null) element.SetAttributeValue("AccessTokenType", "");
                return (String)element.Attribute("AccessTokenType").Value;
            }
            set { element.Attribute("AccessTokenType").Value = value; }
        }

        public String AccessTokenExpire
        {
            get
            {
                if (element.Attribute("AccessTokenExpire") == null) element.SetAttributeValue("AccessTokenExpire", "");
                return (String)element.Attribute("AccessTokenExpire").Value;
            }
            set { element.Attribute("AccessTokenExpire").Value = value; }
        }

        public String RefreshToken
        {
            get
            {
                if (element.Attribute("RefreshToken") == null) element.SetAttributeValue("RefreshToken", "");
                return (String)element.Attribute("RefreshToken").Value;
            }
            set { element.Attribute("RefreshToken").Value = value; }
        }

        public String Active
        {
            get {
                if (element.Attribute("Active") == null) element.SetAttributeValue("Active", "True");                
                return (String)element.Attribute("Active").Value; 
            }
            set { 
                if(String.IsNullOrEmpty(value)) value = "True";
                element.Attribute("Active").Value = value; 
            }
        }

        public DateTime ExpireTime;

        /// <summary>
        /// Towrzenie obiektu na podstawie danych 
        /// </summary>
        /// <param name="parent">rodzic XElementu</param>
        /// <param name="element">XElement z konfiguracją</param>
        public CBConfigFolder(CBConfig parent, XElement element)
        {
            log = new CBLog("CBConfigFolder"); 
            
            this.parent = parent;
            this.element = element;

            this.ExpireTime = new DateTime(0); //DateTime.Now.AddSeconds(3600);

            this.log.Debug("Name = " + this.Name);
            this.log.Debug("SrcFolderName = " + this.SrcFolderName);
            this.log.Debug("DstFolderName = " + this.DstFolderName);
            this.log.Debug("DriverName = " + this.DriverName);
            this.log.Debug("AccessToken = " + this.AccessToken);
            this.log.Debug("AccessTokenType = " + this.AccessTokenType);
            this.log.Debug("AccessTokenExpire = " + this.AccessTokenExpire);
            this.log.Debug("RefreshToken = " + this.RefreshToken);
            this.log.Debug("ExpireTime = " + this.ExpireTime.ToString("yyyy-MM-dd HH:mm:ss"));
            this.log.Debug("Active = " + this.Active);
        }

        /// <summary>
        /// Zapisywanie danych
        /// </summary>
        public void Save()
        {
            log.Info("Zapis konfiguracji: " + this.element);

            this.parent.Save();
        }
    }

    /// <summary>
    /// Konfiguracja drivera
    /// </summary>
    public class CBConfigDriver
    {
        CBLog log;

        CBConfig parent;
        XElement element;

        public String Name
        {
            get { return (String)element.Attribute("Name").Value; }
            set { element.Attribute("Name").Value = value; }
        }

        public String ClientID
        {
            get { return (String)element.Attribute("ClientID").Value; }
            set { element.Attribute("ClientID").Value = value; }
        }

        public String ClientSecret
        {
            get { return (String)element.Attribute("ClientSecret").Value; }
            set { element.Attribute("ClientSecret").Value = value; }
        }

        public String RedirectURL
        {
            get { return (String)element.Attribute("RedirectURL").Value; }
            set { element.Attribute("RedirectURL").Value = value; }
        }

        public String AccessToken
        {
            get { return (String)element.Attribute("AccessToken").Value; }
            set { element.Attribute("AccessToken").Value = value; }
        }

        public String AccessTokenType
        {
            get { return (String)element.Attribute("AccessTokenType").Value; }
            set { element.Attribute("AccessTokenType").Value = value; }
        }

        public String AccessTokenExpire
        {
            get { return (String)element.Attribute("AccessTokenExpire").Value; }
            set { element.Attribute("AccessTokenExpire").Value = value; }
        }

        public String RefreshToken
        {
            get { return (String)element.Attribute("RefreshToken").Value; }
            set { element.Attribute("RefreshToken").Value = value; }
        }

        public DateTime ExpireTime;

        /// <summary>
        /// Towrzenie obiektu na podstawie danych 
        /// </summary>
        /// <param name="parent">rodzic XElementu</param>
        /// <param name="element">XElement z konfiguracją</param>
        public CBConfigDriver(CBConfig parent, XElement element)
        {
            log = new CBLog("CBConfigDriver");

            this.parent = parent;
            this.element = element;

            this.ExpireTime = DateTime.Now.AddSeconds(3600);
            
            this.log.Debug("Name = " + this.Name);
            this.log.Debug("ClientID = " + this.ClientID);
            this.log.Debug("ClientSecret = " + this.ClientSecret);
            this.log.Debug("RedirectURL = " + this.RedirectURL);
            this.log.Debug("AccessToken = " + this.AccessToken);
            this.log.Debug("AccessTokenType = " + this.AccessTokenType);
            //this.log.Debug("AccessTokenExpire = " + this.AccessTokenExpire);
            //this.log.Debug("RefreshToken = " + this.RefreshToken);
            //this.log.Debug("ExpireTime = " + this.ExpireTime.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        /// <summary>
        /// Zapisywanie danych
        /// </summary>
        public void Save()
        {
            log.Info("Zapis konfiguracji: " + this.element);

            this.parent.Save();
        }
    }
}
