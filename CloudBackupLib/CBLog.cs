﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudBackup
{
    /// <summary>
    /// Obsługa logu
    /// </summary>
    public class CBLog
    {
        String name;

        System.IO.StreamWriter file;
        System.Diagnostics.EventLog eventlog;

        String FileName = "";

        /// <summary>
        /// Konstruktor 
        /// </summary>
        /// <param name="name">nazwa dodawana do lini w logu - np. informacja o tym jaka klasa korzysta z logu</param>          
        public CBLog(String name)
        {
            this.name = name;

            //System.Diagnostics.EventLog.CreateEventSource("INNE", "Application");

            //this.eventlog = new System.Diagnostics.EventLog("Application");
            //this.eventlog.Source = "INNE";
            //this.eventlog.WriteEntry("Test log message");

            //this.eventlog.Source = "CloudBackup Service";
            //this.eventlog.Log = "CloudBackupLog"; //"MyLog"; // 

            //if (!System.Diagnostics.EventLog.SourceExists(this.eventlog.Source))
            //{
            //try
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        this.eventlog.Source, this.eventlog.Log);
            //}
            //catch (Exception ex)
            //{

            //}
            //}
            // configure the event log instance to use this source path
            //eventLog1.Source = this.eventlog.Source;

            //eventlog = new EventLog("Application");
            //eventlog.Source = "MySource";
        }

        /// <summary>
        /// Dodanie inforamcji dowolnego typu
        /// </summary>
        /// <param name="message">wiadomość do logu</param>
        /// <param name="type">typ wiadomości</param>
        public void Add(String message, String type="INFO")
        {
            String text = String.Format("{0:u} - {2:s} - {3:s} ", DateTime.Now, type, this.name, message);
            //Console.WriteLine("{0:u} - {1:s} - {2:s}: {3:s} ", DateTime.Now, type, this.path, message);
            Console.WriteLine(text);

            //this.FileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log."+DateTime.Today+".txt");
            //this.FileName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "log."+DateTime.Today+".txt");
            //this.FileName = "log." + DateTime.Today + ".txt";
            //System.IO.StreamWriter file = new System.IO.StreamWriter(FileName);
            //file.WriteLine(text);
            //file.Close();

            //this.eventlog.WriteEntry(text);
        }

        /// <summary>
        /// Dodanie wiadomości typu INFO
        /// </summary>
        /// <param name="message"></param>
        public void Info(String message)
        {
            this.Add(message, "INFO");
        }

        /// <summary>
        /// Dodanie wiadomości typu DEBUG
        /// </summary>
        /// <param name="message"></param>
        public void Debug(String message)
        {
            this.Add(message, "DEBUG");
        }
    }
}
