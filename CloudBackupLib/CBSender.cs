﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudBackup
{
    /// <summary>
    /// System pobierania drivera - (przenieść to do innej klasy)
    /// </summary>
    class CBSender
    {
        CBLog log;

        public CBSender()
        {
            this.log = new CBLog("CBSender");
        }

        /// <summary>
        /// Nieużywane
        /// </summary>
        public void Start()
        {
            this.log.Info("Start");
        }

        /// <summary>
        /// Nieużywane
        /// </summary>
        public void Stop()
        {
            this.log.Info("Stop");
        }

        /// <summary>
        /// Pobieranie odpowiwedneig drivera na podstawie nazwy
        /// </summary>
        /// <param name="name">nazwa drivera</param>
        /// <returns></returns>
        public CBDriver GetDriver(String name="Dropbox")
        {
            this.log.Info("GetDriver");

            if (name == "Dropbox") 
            {
                return new CBDriverDropbox();
            }

            if (name == "Google")
            {
                return new CBDriverGoogle();
            }

            if (name == "Hubic")
            {
                return new CBDriverHubic();
            }

            return new CBDriver();
        }
    }
}
