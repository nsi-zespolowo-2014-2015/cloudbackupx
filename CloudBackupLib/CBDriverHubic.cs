﻿using System;
using System.Collections.Generic; // List
using System.Net;  // WebRequest, WebResponse
using System.Text; // Encoding
using System.IO;   // Stream

using Newtonsoft.Json; // JsonConvert.DeserializeObject
using Newtonsoft.Json.Linq;

/*
 * UWAGA: redirectUrl musi zawierać / na końcu czyli http://localhost/
 */
namespace CloudBackup
{
    /// <summary>
    /// Klasa odpowiedzialna za komunikację z dyskiem HubiC
    /// </summary>
    public class CBDriverHubic : CBDriver
    {
        String OpenStackToken = "";
        String OpenStackEndpoint = "";
        String OpenStackExpire = "";

        public CBDriverHubic(CBConfigDriver config = null)
        {
            this.name = "Hubic";
            this.log = new CBLog("CBDriver" + name);

            this.config = config;

            this.authUrl = "https://api.hubic.com/oauth/auth/";
            this.tokenUrl = "https://api.hubic.com/oauth/token/";
            this.commandUrl = "https://api.hubic.com/1.0";

            this.log.Debug("constructor()");

            // obejscie problemu z nieporawnymi certyfikatami
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            //this.config = config;
            //GetConfig();

            if (config != null && config.ExpireTime < DateTime.Now)
            {
                RefreshToken(config.RefreshToken);
            }
        }

        //---------------------------------------------------------------------------------------

        public override String[] GetCommands()
        {
            String[] commands = {
                "/account",
                "/account/credentials"
            };

            return commands;
        }

        //---------------------------------------------------------------------------------------
        // Pobieranie adresu do strony autoryzacji dostępu aplikacji do konta użytkownika
        //---------------------------------------------------------------------------------------

        public override String GetAuthorizationUrl()
        {
            /*
https://api.hubic.com/oauth/auth/?
client_id=api_hubic_1366206728U6faUvDSfE1iFImoFAFUIfDRbJytlaY0
&redirect_uri=https%3A%2F%2Fapi.hubic.com%2Fsandbox%2F
&scope=usage.r,account.r,getAllLinks.r,credentials.r,sponsorCode.r,activate.w,sponsored.r,links.drw
&response_type=code
&state=RandomString_5q0w0m0eqs
             */

            String funcName = "GetAuthorizationUrl()";

            this.log.Debug(funcName + ": [BEGIN]");

            List<String> data = new List<String>();
            data.Add("client_id=" + config.ClientID);
            data.Add("redirect_uri=" + "http://localhost/"); //config.RedirectURL); // 
            data.Add("response_type=" + "code");
            data.Add("scope=" + "usage.r,account.r,getAllLinks.r,credentials.r,sponsorCode.r,activate.w,sponsored.r,links.drw");
            data.Add("state=" + "moj_state");

            String url = authUrl + "?" + string.Join("&", data.ToArray());

            this.log.Debug("url: " + url);

            this.log.Debug(funcName + ": [END]");

            return url;
        }

        public override String GetToken(String code)
        {
/*
Credentials :
api_hubic_1366206728U6faUvDSfE1iFImoFAFUIfDRbJytlaY0:gXfu3KUIO1K57jUsW7VgKmNEhOWIbFdy7r8Z2xBdZn5K6SMkMmnU4lQUcnRy5E26

Base64 : YXBpX2h1YmljXzEzNjYyMDY3MjhVNmZhVXZEU2ZFMWlGSW1vRkFGVUlmRFJiSnl0bGFZMDpnWGZ1M0tVSU8xSzU3alVzVzdWZ0ttTkVoT1dJYkZkeTdyOFoyeEJkWm41SzZTTWtNbW5VNGxRVWNuUnk1RTI2

And create a POST request :

POST https://api.hubic.com/oauth/token/ HTTP/1.1
Authorization: Basic YXBpX2h1YmljXzEzNjYyMDY3MjhVNmZhVXZEU2ZFMWlGSW1vRkFGVUlmRFJiSnl0bGFZMDpnWGZ1M0tVSU8xSzU3alVzVzdWZ0ttTkVoT1dJYkZkeTdyOFoyeEJkWm41SzZTTWtNbW5VNGxRVWNuUnk1RTI2

code=1430996863zvfNBBnKTQ0XkcbEh26R3jOjVtPmG3GKfy9Evt2Pl2KVxJ2xSC4ybbahgY5xEqq8
&redirect_uri=https://api.hubic.com/sandbox/
&grant_type=authorization_code
 */
            String funcName = "GetToken()";

            this.log.Debug(funcName + ": [BEGIN]");
            this.log.Debug(funcName + ": code: " + code);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(config.ClientID+":"+config.ClientSecret)));

            Dictionary<String, String> postData = new Dictionary<String, String>();
            postData.Add("code", code);
            postData.Add("grant_type", "authorization_code");
            postData.Add("redirect_uri", config.RedirectURL);

            //String result = POST(tokenUrl, null, postData, null, null);
            String result = REQUEST("POST", tokenUrl, null, postData, headers, null);

            this.log.Debug(funcName + ": result: " + result);

            //----------
            if (result != null && result != "")
            {
                this.log.Debug(funcName + ": result -> json");

                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                if (json.GetValue("access_token") != null && json.GetValue("access_token").ToString() != "")
                {
                    config.AccessToken = json.GetValue("access_token").ToString();
                    config.AccessTokenType = json.GetValue("token_type").ToString();
                    config.AccessTokenExpire = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).ToString();
                    config.RefreshToken = json.GetValue("refresh_token").ToString();

                    config.Save();
                }

                this.log.Debug(funcName + ": config saved");

                GetCredential();
            }
            //-------

            this.log.Debug(funcName + ": [END]");

            return result;
        }

        public override String RefreshToken(String refreshToken)
        {
/*
POST https://api.hubic.com/oauth/token/ HTTP/1.1
Authorization: Basic YXBpX2h1YmljXzEzNjYyMDY3MjhVNmZhVXZEU2ZFMWlGSW1vRkFGVUlmRFJiSnl0bGFZMDpnWGZ1M0tVSU8xSzU3alVzVzdWZ0ttTkVoT1dJYkZkeTdyOFoyeEJkWm41SzZTTWtNbW5VNGxRVWNuUnk1RTI2

refresh_token=ewoVnTIT5jVK5CoWLOkgZIvYRZQXcbQPUyFbCwku2QNnf3fIFDRuLsZrDAPRNgH6
&grant_type=refresh_token
 */

            this.log.Debug("RefreshToken(): [BEGIN]");
            this.log.Debug("RefreshToken(): refreshToken: " + refreshToken);

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(config.ClientID + ":" + config.ClientSecret)));

            Dictionary<String, String> postData = new Dictionary<String, String>();
            postData.Add("grant_type", "refresh_token");
            postData.Add("refresh_token", config.RefreshToken);

            //String result = POST(tokenUrl, null, postData, headers, null);
            String result = REQUEST("POST", tokenUrl, null, postData, headers, null);

            this.log.Debug("RefreshToken(): result: " + result);
            this.log.Debug("RefreshToken(): [END]");

            //----------
            if (result != null && result != "")
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                if (json.GetValue("access_token") != null && json.GetValue("access_token").ToString() != "")
                {
                    config.AccessToken = json.GetValue("access_token").ToString();
                    config.AccessTokenType = json.GetValue("token_type").ToString();
                    config.AccessTokenExpire = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).Ticks.ToString();
                    config.ExpireTime = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString()));

                    config.Save();
                }

                GetCredential();
            }

            //-------
            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        //------------------------------------------------------------------------
        //------------------------------------------------------------------------

        //------------------------------------------------------------------------
        // FOLDERS
        //------------------------------------------------------------------------

        //TODO: niezrobione
        public override String CreateFolder(String name, String parentID = "root")
        {
            String funcName = "CreateFolder()";

            if (name.StartsWith("/"))
            {
                name = name.Substring(1);
            }

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parentID);

            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);
            headers.Add("Content-Type", "application/directory");
            headers.Add("Content-Length", "0");

            String result = REQUEST("PUT", this.OpenStackEndpoint + "/" + container + "/" + name, null, null, headers, null);

            this.log.Debug(funcName + ": PUT " + this.OpenStackEndpoint + "/" + container + "/" + name);

            //JObject json = JsonConvert.DeserializeObject<JObject>(result);

            //if (json.GetValue("error") != null || json.GetValue("id") == null)
            //{
            //    return result;
            //}
            //else
            //{
            //    parentID = json.GetValue("id").ToString();
            //}

            return result;
        }

        //TODO: czesciowo zrobione
        public override String RenameFolder(String oldPath, String newPath, String newName, String parentID = "root")
        {
            String funcName = "RenameFolder()";

            if (oldPath.StartsWith("/"))
            {
                oldPath = oldPath.Substring(1);
            }

            if (newPath.StartsWith("/"))
            {
                newPath = newPath.Substring(1);
            }

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parentID);

            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);
            headers.Add("Content-Type", "application/directory");
            headers.Add("X-Copy-From", container + "/" + oldPath);

            headers.Add("Content-Length", "0");

            String result = REQUEST("PUT", this.OpenStackEndpoint + "/" + container + "/" + newPath, null, null, headers, null);

            this.log.Debug(funcName + ": PUT " + this.OpenStackEndpoint + "/" + container + "/" + newPath);
            this.log.Debug(funcName + ": PUT: X-Copy-From: " + this.OpenStackEndpoint + "/" + container + "/" + oldPath);

            DeleteFolder(oldPath, parentID);

            return result;
        }


        public override String DeleteFolder(String name, String parentID = "root")
        {
            String funcName = "DeleteFolder()";

            if (name.StartsWith("/"))
            {
                name = name.Substring(1);
            }

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parentID);

            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);

            String result = REQUEST("DELETE", this.OpenStackEndpoint + "/" + container + "/" + name, null, null, headers, null);

            this.log.Debug(funcName + ": DELETE " + this.OpenStackEndpoint + "/" + container + "/" + name);

            //JObject json = JsonConvert.DeserializeObject<JObject>(result);

            //if (json.GetValue("error") != null || json.GetValue("id") == null)
            //{
            //    return result;
            //}
            //else
            //{
            //    parentID = json.GetValue("id").ToString();
            //}

            return result;
        }

        //------------------------------------------------------------------------
        // FILES
        //------------------------------------------------------------------------

        //TODO: niezrobione
        public override String UploadFile(String filename, String remoteFilename, String parentID = "root")
        {
            String id = GetID(remoteFilename, parentID);

            byte[] rawData;

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("uploadType", "media");

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);
            headers.Add("Content-Type", "application/octet-stream");

            try
            {
                rawData = File.ReadAllBytes(filename);
            }
            catch (Exception ex)
            {
                Dictionary<String, String> jsonData = new Dictionary<String, String>();
                jsonData.Add("error", "true");
                jsonData.Add("path", "CBDriverHubic.UploadFile()");
                jsonData.Add("message", ex.Message);
                jsonData.Add("StatusCode", "0");

                // zwrocenie bledu
                return JsonConvert.SerializeObject(jsonData);
            }

            //String result = POST(commandUrl + "/upload/drive/v2/files", query, null, headers, data);
            String result = REQUEST("POST", commandUrl + "/upload/drive/v2/files", getData, null, headers, rawData);

            //TODO: nadanie nazwy zewnętrznej

            return result;
        }

        //TODO: czesciowo zrobione
        public override String UploadFile2(String filename, String remoteFilename, String parentID = "root")
        {
            String funcName = "UploadFile2()";

            if (remoteFilename.StartsWith("/"))
            {
                remoteFilename = remoteFilename.Substring(1);
            }

            this.log.Debug(funcName + ": filename: " + filename);
            this.log.Debug(funcName + ": remoteFilename: " + remoteFilename);
            this.log.Debug(funcName + ": parent: " + parentID);


            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);
            headers.Add("Content-Type", "application/octet-stream");

            byte[] rawData;

            try
            {
                rawData = File.ReadAllBytes(filename);
                //headers.Add("Content-Length", rawData.Length.ToString());
            }
            catch (Exception ex)
            {
                Dictionary<String, String> jsonData = new Dictionary<String, String>();
                jsonData.Add("error", "true");
                jsonData.Add("path", "CBDriverHubic.UploadFile2()");
                jsonData.Add("message", ex.Message);
                jsonData.Add("StatusCode", "0");

                // zwrocenie bledu
                return JsonConvert.SerializeObject(jsonData);
            }

            String result = REQUEST("PUT", this.OpenStackEndpoint + "/" + container + "/" + remoteFilename, null, null, headers, rawData);

            this.log.Debug(funcName + ": PUT " + this.OpenStackEndpoint + "/" + container + "/" + remoteFilename);

            return result;
        }

        //TODO: czesciowo zrobione
        public override String RenameFile(String oldPath, String newPath, String newName, String parentID = "root")
        {
            String funcName = "RenameFile()";

            if (oldPath.StartsWith("/"))
            {
                oldPath = oldPath.Substring(1);
            }

            if (newPath.StartsWith("/"))
            {
                newPath = newPath.Substring(1);
            }

            this.log.Debug(funcName + ": oldPath: " + oldPath);
            this.log.Debug(funcName + ": newPath: " + newPath);
            this.log.Debug(funcName + ": newName: " + newName);
            this.log.Debug(funcName + ": parent: " + parentID);

            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);
            headers.Add("X-Copy-From", container + "/" + oldPath);

            headers.Add("Content-Length", "0");

            String result = REQUEST("PUT", this.OpenStackEndpoint + "/" + container + "/" + newPath, null, null, headers, null);

            this.log.Debug(funcName + ": PUT " + this.OpenStackEndpoint + "/" + container + "/" + newPath);
            this.log.Debug(funcName + ": PUT: X-Copy-From: " + this.OpenStackEndpoint + "/" + container + "/" + oldPath);

            DeleteFile(oldPath, parentID);

            return result;
        }

        
        public override String DeleteFile(String name, String parentID = "root")
        {
            String funcName = "DeleteFile()";

            if (name.StartsWith("/"))
            {
                name = name.Substring(1);
            }

            this.log.Debug(funcName + ": path: " + name);
            this.log.Debug(funcName + ": parent: " + parentID);

            CheckCredential();

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);

            String result = REQUEST("DELETE", this.OpenStackEndpoint + "/" + container + "/" + name, null, null, headers, null);

            this.log.Debug(funcName + ": DELETE " + this.OpenStackEndpoint + "/" + container + "/" + name);

            return result;
        }

        //TODO: niezrobione
        public override String DownloadFile(String id, String filename = "tmp")
        {
            //TODO: niezrobione

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/drive/v2/files/" + id, null, null, headers, null);

            return result;
        }

        //------------------------------------------------------------------------
        // INNE
        //------------------------------------------------------------------------

        public override String ListFolder(String name, String parent = "")
        {
            //TODO: obsluga kontenerow ?!

            String container = "default";

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("X-Auth-Token", this.OpenStackToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            getData.Add("path", name); // startowy katalog to nie "/" tylko pusty napis ""
                                       // brak (nawet pustego) "path" w zapytaniu listuje rekurencyjnie
            //getData.Add("prefix", path); // nazwa (sub)fodlera
            //getData.Add("delimiter", path); // oddzielacz (sub)folderow - "/"
            //getData.Add("format", "json"); // "" = plaintext, "json", "xml"

            String result = REQUEST("GET", this.OpenStackEndpoint + container, getData, null, headers, null);

            return result;
        }

        //TODO: niezrobione
        public override String FindFiles(String name, String parentID = "root")
        {
            String[] elements = name.Split("/".ToCharArray());

            foreach (String i in elements)
            {
                String r = Find(i, parentID);

                dynamic json = JsonConvert.DeserializeObject(r);

                if (json["items"] == null || json["items"].Count == 0)
                {
                    return "";
                }

                parentID = json["items"][0]["id"];

                this.log.Debug("FindFiles: path: " + i);
                this.log.Debug("FindFiles: parentID: " + parentID);
                this.log.Debug("FindFiles: id: " + json["items"][0]["id"]);
            }

            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            Dictionary<String, String> getData = new Dictionary<String, String>();
            //getData.Add("mimeType='application/vnd.google-apps.folder'");
            //getData.Add("q", "title contains '" + title + "'");
            getData.Add("q", "title='" + name + "' and '" + parentID + "' in parents");

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", getData, null, headers, null);
            //String result = GET("http://httpbin.org/get", getData, null, headers, null);

            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        //TODO: niezrobione
        public override String ListFiles()
        {
            Dictionary<String, String> headers = new Dictionary<String, String>();
            headers.Add("Authorization", config.AccessTokenType + " " + config.AccessToken);

            String result = REQUEST("GET", commandUrl + "/drive/v2/files", null, null, headers, null);

            //return JsonConvert.DeserializeObject(result);
            return result;
        }

        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        // OpenStack
        //------------------------------------------------------------------------
        //------------------------------------------------------------------------

        /*
         * Pobieranie uprawnień do OpenStackAPI
         * - token
         * - url (endpoint)
         */
        public override String GetCredential()
        {
            String result = Get("/account/credentials", config.AccessTokenType + " " + config.AccessToken);

            if(!result.Equals(""))
            {
                try
                {
                    JObject json = JsonConvert.DeserializeObject<JObject>(result);
                    this.OpenStackToken = json.GetValue("token").ToString();
                    this.OpenStackEndpoint = json.GetValue("endpoint").ToString();
                }
                catch (Exception ex)
                {
                    this.log.Debug("GetCredential(): ex: " + ex.Message);
                    this.log.Debug("GetCredential(): result: " + result);
                }
            }

            return result;
        }

        public String CheckCredential()
        {
            if (this.OpenStackEndpoint == "")
            {
                return GetCredential();
            }

            return "";
        }
    }
}
