﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Configuration;

namespace CloudBackup
{
    /// <summary>
    /// System uruchamiania watcherów na podstawie konfiguracji folderów
    /// </summary>
    public class CBMonitor
    {
        CBLog log;

        CBConfig config;

        List<String> DriversNames;

        List<CBConfigFolder> FoldersConfig;
        //List<CBConfigDriver> DriversConfig;

        List<CBWatcher> watchersList;

        /// <summary>
        /// Tworzenie wszystkich watcherow na podstawie pliku konfiguracyjnego
        /// </summary>
        public CBMonitor()
        {
            this.log = new CBLog("CBMonitor");

            //-------------------------------------

            this.config = new CBConfig();

            // lista wszystkich dostepnych driverow
            this.DriversNames = config.GetDriversNames();

            this.log.Debug("DriversNames = " + DriversNames);

            foreach (String el in DriversNames)
            {
                this.log.Debug("driver path: " + el);
            }

            this.watchersList = new List<CBWatcher>();
        }

        /// <summary>
        /// Tworzenie wszystkich watcherow na podstawie pliku konfiguracyjnego
        /// </summary>
        public void Start()
        {
            CBWatcher w;

            this.log.Info("Start(): [BEGIN]");

            this.FoldersConfig = config.GetAllFolderConfigs();

            foreach (CBConfigFolder el in FoldersConfig)
            {
                if (el.Active == "True")
                {
                    try{
                        if (!System.IO.Directory.Exists(el.SrcFolderName))
                        {
                            System.IO.DirectoryInfo info = System.IO.Directory.CreateDirectory(el.SrcFolderName);
                        }
                        w = new CBWatcher(el, config.GetDriverConfig(el.DriverName));
                        this.watchersList.Add(w);
                        w.Start();
                        this.log.Info("Start(): Watcher stworzony dla: " + el.Name + " [ " + el.SrcFolderName + " => " + el.DriverName + ":" + el.DstFolderName + " ]");
                    }
                    catch(Exception ex)
                    {
                        this.log.Info("Start(): problem z katalogiem zrodlowym w: " + el.Name + " [ " + el.SrcFolderName + " => " + el.DriverName + ":" + el.DstFolderName + " ]: " + ex.Message);
                    }
                }
                else
                {
                    this.log.Info("Start(): Watcher *pominiety* dla: " + el.Name + " [ " + el.SrcFolderName + " => " + el.DriverName + ":" + el.DstFolderName + " ]");
                }
            }

            this.config.Save();

            this.log.Info("Start(): [END]");
        }

        /// <summary>
        /// Zatrzymywanie wszystkich watcherow
        /// </summary>
        public void Stop()
        {
            this.log.Info("Stop(): [BEGIN]");

            foreach (CBWatcher w in this.watchersList)
            {
                w.Stop();
            }

            this.log.Info("Stop(): [END]");
        }
    }
}
