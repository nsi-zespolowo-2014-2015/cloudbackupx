﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Forms; // OpenFileDialog

// program wymaga Json.NET (4.5)
// - lub -
// PM> Install-Package Newtonsoft.Json

using Newtonsoft.Json; // JsonConvert.DeserializeObject
using Newtonsoft.Json.Linq;

using CloudBackup;

namespace TestDriverAPI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CBDriversManager driversManager;
        public CBDriver driver;
        public CBConfig config;

        public CBLog log;

        public MainWindow()
        {
            this.log = new CBLog("MainWindow");

            String funcName = "Constructor()";

            this.log.Debug(funcName + ": [BEGIN]");
            
            //--------------------------------------

            InitializeComponent();

            //--------------------------------------

            texboxWynikURL.TextWrapping = TextWrapping.Wrap;

            // wypelnienie listy typow tokena autoryzacyjnego
            // - wlasciwie znam tylko jeden rodzaj - Bearer

            comboboxTokenType.Items.Add("Bearer");
            comboboxTokenType.SelectedIndex = 0;

            //--------------------------------------
            // musi powstawc przed comboboxSelectDriver.SelectedIndex = 0;
            // bo to wywola funkcje, ktora wymaga juz instniejace `config`

            config = new CBConfig();

            //--------------------------------------
            // pobranie listy dostepnych driverow
            //--------------------------------------

            driversManager = new CBDriversManager();

            List<String> driversNames = driversManager.GetDriversNames();

            foreach (String driveName in driversNames)
            {
                this.log.Debug(funcName + ": " + driveName);
                comboboxDriversNames.Items.Add(driveName);
                comboboxSelectDriver.Items.Add(driveName);
            }

            comboboxDriversNames.SelectedIndex = 0;
            comboboxSelectDriver.SelectedIndex = 0;

            //--------------------------------------
            // pobranie konfiguracji drivera i jego konfiguracji
            // nastapi pod wplywem comboboxSelectDriver.SelectedIndex = 0;
            //--------------------------------------

            // --- nic tu nie trzeba ---

            //--------------------------------------
            // pobranie listy dostepnych driverow
            //--------------------------------------

            List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();

            foreach (CBConfigFolder folder in foldersConfigs)
            {
                this.log.Debug(funcName + ": " + folder.Name + " [ " + folder.SrcFolderName + " => " + folder.DstFolderName + " ](" + folder.DriverName + ")");
                comboboxFolders.Items.Add(folder.Name);
            }

            comboboxDriversNames.SelectedIndex = 0;
            comboboxSelectDriver.SelectedIndex = 0;

            //--------------------------------------

            this.log.Debug(funcName + ": [END]");
        }

        private void comboboxSelectDriver_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String funcName = "comboboxSelectDriver_SelectionChanged()";

            this.log.Debug(funcName + ": [BEGIN]");

            String selected = (String)comboboxSelectDriver.SelectedValue;

            this.log.Debug(funcName + ": selectedValue: " + selected);

            switch (selected)
            {
                case "Google":
                    this.log.Debug(funcName + ": case: Google");
                    driver = new CBDriverGoogle();
                    driver.config = config.GetDriverConfig("Google");
                    break;

                case "Dropbox":
                    this.log.Debug(funcName + ": case: Dropbox");
                    driver = new CBDriverDropbox();
                    driver.config = config.GetDriverConfig("Dropbox");
                    break;

                case "Hubic":
                    this.log.Debug(funcName + ": case: Hubic");
                    driver = new CBDriverHubic();
                    driver.config = config.GetDriverConfig("Hubic");
                    break;
            }

            textboxRefreshToken.Text = driver.config.RefreshToken;
            textboxAccessToken.Text = driver.config.AccessToken;

            texboxWynikURL.Clear();
            textboxKod.Clear();
            textboxWynikToken.Clear();
            textboxWynikKomenda.Clear();

            // wypelnieni listy dostepnych komend (GET)

            comboboxKomenda.Items.Clear();
            foreach (String item in driver.GetCommands())
            {
                comboboxKomenda.Items.Add(item);
            }
            comboboxKomenda.SelectedIndex = 0;

            this.log.Debug(funcName + ": [END]");
        }

        //--------------------------------------------------
        // AUTORYZACJA
        //--------------------------------------------------

        private void buttonPobierzURL_Click(object sender, RoutedEventArgs e)
        {

            String result = driver.GetAuthorizationUrl();

            if (result.Equals(""))
            {
                texboxWynikURL.Text = "- brak -";
            }
            else
            {
                texboxWynikURL.Text = result;
            }
        }

        private void buttonPobierzToken_Click(object sender, RoutedEventArgs e)
        {
            String result = driver.GetToken(textboxKod.Text);

            if (result.Equals(""))
            {
                textboxWynikToken.Text = "- brak -";
            }
            else
            {
                try
                {
                    textboxWynikToken.Text = result;

                    dynamic json = JsonConvert.DeserializeObject(result);

                    textboxAccessToken.Text = json.GetValue("access_token").ToString();
                    textboxRefreshToken.Text = json.GetValue("refresh_token").ToString();

                    driver.config.AccessToken = json.GetValue("access_token").ToString();
                    // nie kazde API uzywa "refresh_token"
                    driver.config.RefreshToken = json.GetValue("refresh_token").ToString();

                    driver.config.Save();

                    // dla hubic - dostep do OpenStackAPI
                    driver.GetCredential();
                }
                catch(Exception ex)
                {
                    this.log.Debug(ex.Message);
                }
            }
        }

        private void buttonOdswiezToken_Click(object sender, RoutedEventArgs e)
        {
            String result = driver.RefreshToken(textboxRefreshToken.Text);

            if (result.Equals(""))
            {
                textboxWynikToken.Text = "- brak -";
            }
            else
            {
                textboxWynikToken.Text = result;
                driver.config.RefreshToken = textboxRefreshToken.Text;

                textboxWynikToken.Text = result;
                this.log.Debug("MainWindow.buttonOdswiezToken_Click(): result: " + result);

                if (result == "")
                {
                    //TODO: okienko z komunikatem o problemie
                    this.log.Debug("MainWindow.buttonOdswiezToken_Click(): result: " + result);
                }
                else
                {
                    JObject json = JsonConvert.DeserializeObject<JObject>(result);

                    if (json.GetValue("access_token") == null || json.GetValue("access_token").ToString() == "")
                    {
                        //TODO: okienko z komunikatem o problemie
                        this.log.Debug("MainWindow.buttonOdswiezToken_Click(): json.GetValue(\"access_token\"): " + json.GetValue("access_token"));
                    }
                    else
                    {
                        this.log.Debug("MainWindow.buttonOdswiezToken_Click(): zapisywanie do pliku konfiguracyjnego");

                        // aktualizacja AuthToken w oknie 
                        textboxAccessToken.Text = json.GetValue("access_token").ToString();
                        //textboxRefreshToken.Text = json.GetValue("refresh_token").ToString();

                        // aktualizacja AuthToken w driverze
                        driver.config.AccessToken = json.GetValue("access_token").ToString();
                        driver.config.AccessTokenExpire= DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).ToString();
                        //driver.config.element.RefreshToken = json.GetValue("refresh_token").ToString();

                        // aktualizacja AuthToken w pliku konfiguracyjnym
                        driver.config.AccessToken = json.GetValue("access_token").ToString();
                        driver.config.AccessTokenExpire = DateTime.Now.AddSeconds(Int16.Parse(json.GetValue("expires_in").ToString())).ToString();
                        //driver.config.element.RefreshToken = json.GetValue("refresh_token").ToString();

                        driver.config.Save();
                    }
                }
            }
        }

        //--------------------------------------------------
        // KONFIGURACJA
        //--------------------------------------------------

        private void comboboxDriversNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String funcName = "comboboxFolders_SelectionChanged()";

            String selected = (String)comboboxDriversNames.SelectedValue;

            this.log.Debug(funcName + ": selected: " + selected);

            try
            {
                CBConfigDriver tempDriverConfig = driversManager.GetDriverConfig((String)comboboxDriversNames.SelectedValue);

                textboxClientID.Text = tempDriverConfig.ClientID;
                textboxClientSecret.Text = tempDriverConfig.ClientSecret;
                textboxConfigAuthToken.Text = tempDriverConfig.AccessToken;
                textboxConfigRefreshToken.Text = tempDriverConfig.RefreshToken;
            } 
            catch(Exception ex)
            {
                this.log.Debug(funcName + ":  ex.Message: " + ex.Message);
            }

            this.log.Debug(funcName + ": [END]");
        }

        private void comboboxFolders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String funcName = "comboboxFolders_SelectionChanged()";

            this.log.Debug(funcName + ": [BEGIN]");

            String selected = (String)comboboxFolders.SelectedValue;

            this.log.Debug(funcName + ": selectedValue: " + selected);

            List<CBConfigFolder> foldersConfigs = config.GetAllFolderConfigs();

            foreach (CBConfigFolder folder in foldersConfigs)
            {
                if (folder.Name == selected)
                {
                    textboxFolderNazwa.Text = folder.Name;
                    textboxFolderDriver.Text = folder.DriverName;
                    textboxFolderSrcFolder.Text = folder.SrcFolderName;
                    textboxFolderDstFolder.Text = folder.DstFolderName;
                }
            }

            this.log.Debug(funcName + ": [END]");
        }

        //--------------------------------------------------
        // OGÓLNE
        //--------------------------------------------------

        private void buttonWykonajKomende_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String result = driver.Get(comboboxKomenda.Text, comboboxTokenType.Text + " " + textboxAccessToken.Text);

            textboxWynikKomenda.AppendText("Token: " + comboboxTokenType.Text + " " + textboxAccessToken.Text + "\n");
            textboxWynikKomenda.AppendText("Komenda: GET " + comboboxKomenda.Text + "\n\n");

            if (result.Equals(""))
            {
                textboxWynikKomenda.AppendText("- brak -");
            }
            else
            {
                textboxWynikKomenda.AppendText(result);
            }

            if (comboboxKomenda.Text == "/account/credentials")
            {
                textboxWynikKomenda.AppendText("\n\n");

                String container = "/default"; // "/default_segments"
                
                JObject json = JsonConvert.DeserializeObject<JObject>(result);
                String token = json.GetValue("token").ToString();
                String endpoint = json.GetValue("endpoint").ToString();
                textboxWynikKomenda.AppendText("token: " + token + "\n");
                textboxWynikKomenda.AppendText("endpoint: " + endpoint + container + "\n");
                textboxWynikKomenda.AppendText("\n");

                Dictionary<String, String> headers = new Dictionary<String, String>();
                headers.Add("X-Auth-Token", token);

                Dictionary<String, String> getData = new Dictionary<String, String>();
                //getData.Add("format", "json");
                getData.Add("path", "");

                result = driver.REQUEST("GET", endpoint + container, getData, null, headers, null);

                textboxWynikKomenda.AppendText(result + "\n\n");
            }
        }

        //--------------------------------------------------
        // FILES
        //--------------------------------------------------

        private void buttonUploadFile_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() < 2)
            {
                textboxWynikKomenda.AppendText("- wymagane dwa argumenty -");
                return;
            }

            String result = driver.UploadFile2(args[0], args[1]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonRenameFile_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() < 3)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -\n1: oldPath,\n2: newPath\n3: newName\n");
                return;
            }

            String result = driver.RenameFile(args[0], args[1], args[2]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonDeleteFile_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() == 0)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -");
                return;
            }

            String result = driver.DeleteFile(args[0]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonDownloadFile_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() == 0)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -");
                return;
            }

            String result = driver.DownloadFile(args[0]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku-" : result);
        }

        private void buttonListFiles_Click(object sender, RoutedEventArgs e)
        {
            String result = driver.ListFiles();

            if (result != "")
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(result);

                textboxWynikKomenda.Clear();

                if (json.Property("error") != null)
                {
                    textboxWynikKomenda.AppendText(json.Property("error").ToString());
                }
                else
                {
                    foreach (dynamic o in json["items"])
                    {
                        textboxWynikKomenda.AppendText(o["createdDate"] + " | " + o["id"] + " | " + o["title"] + "\n");
                    }
                }
            }
            else
            {
                textboxWynikKomenda.AppendText(result.Equals("") ? "- brak -" : result);
            }
        }

        //--------------------------------------------------
        // FOLDERS
        //--------------------------------------------------

        private void buttonCreateFolder_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() == 0)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -");
                return;
            }

            String result = driver.CreateFolder(args[0]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonRenameFolder_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() < 3)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -\n1: oldPath,\n2: newPath\n3: newName\n");
                return;
            }

            String result = driver.RenameFolder(args[0], args[1], args[2]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);

        }

        private void buttonDeleteFolder_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() == 0)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -");
                return;
            }

            String result = driver.DeleteFolder(args[0]);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonListFolders_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String result = driver.ListFolder(textboxFileName.Text);

            //if (result != "")
            //{
            //    JObject json = JsonConvert.DeserializeObject<JObject>(result);

            //    textboxWynikKomenda.Clear();

            //    if (json.Property("error") != null)
            //    {
            //        textboxWynikKomenda.AppendText(json.Property("error").ToString();
            //    }
            //    else
            //    {
            //        foreach (dynamic o in json["items"])
            //        {
            //            textboxWynikKomenda.AppendText(o["createdDate"] + " | " + o["id"] + " | " + o["title"] + "\n";
            //        }
            //    }
            //}
            //else
            //{
                textboxWynikKomenda.AppendText(result.Equals("") ? "- brak -" : result);
            //}
        }

        private void buttonFindFiles_Click(object sender, RoutedEventArgs e)
        {
            textboxWynikKomenda.Clear();

            String[] args = textboxFileName.Text.Split(" \t".ToArray());

            if (args.Count() == 0)
            {
                textboxWynikKomenda.AppendText("- brak argumentow -");
                return;
            }

            String result = driver.FindFiles(textboxFileName.Text);

            textboxWynikKomenda.AppendText(result.Equals("") ? "- brak wyniku -" : result);
        }

        private void buttonFileDialog_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                textboxFileName.Text = dialog.FileName;
            }
        }

    }
}
